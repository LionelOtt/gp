add_library( gtest src/gtest-all.cc )
target_link_libraries( gtest pthread )
add_library( gtest_main src/gtest_main.cc )
target_link_libraries( gtest_main pthread )
