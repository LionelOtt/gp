#ifndef _Qt_TA_INTERFACE_H_
#define _Qt_TA_INTERFACE_H_

#include <QtGui/QMainWindow>
#include "ui_qt_template_app_interface.h"
#include "../template_app_generic_interface.h"

class TemplateAppCore;

class QtTAInterface
    : public QMainWindow
    , public TAGenericInterface
{
    Q_OBJECT

public:
    QtTAInterface(TemplateAppCore* core, QWidget *parent = 0, Qt::WFlags flags = 0);
    ~QtTAInterface();

    void update();
    void create_qt_connections();

    signals:
        void update_signal();

private:
        Ui::QtTAInterface ui;
};


#endif //_Qt_TA_INTERFACE_H_
