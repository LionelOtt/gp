#include <apps/regression_app/qt_interface/qt_3d_plot_widget.h>
#include <apps/regression_app/qt_interface/qt_regression_app_interface.h>
#include <iostream>

using namespace std;

using namespace Qwt3D;


Qt3DPlotWidget::Qt3DPlotWidget(QtRAInterface *interface, QString name)
    :m_x_max(20),
    m_x_min(-20),
    m_y_max(20),
    m_y_min(-20),
    m_z_min(-10),
    m_resolution(0.1),
    m_name(name)
{
    m_interface = interface;
}

Qt3DPlotWidget::~Qt3DPlotWidget()
{

}

void Qt3DPlotWidget::set_sample_data(
        Eigen::MatrixXd const&  data,
        Eigen::VectorXd const&  labels
        )
{

}

void Qt3DPlotWidget::set_regression_data(Eigen::ArrayXd const& data_matrix)
{
    setTitle(m_name);
    coordinates()->setNumberFont(QFont("Verdana", 10));
    coordinates()->setLabelFont(QFont("Verdana", 10, QFont::Bold));
    setTitleFont( "Verdana", 14, QFont::Normal );
    if (data_matrix.rows()<=3)
        return;

    unsigned int n_div = 0;
    double min = 0;
    double max = 0;
    double resolution = 0;

    m_interface->get_regression_domain_params(n_div,min,max,resolution);

    double** raw_data         = new double* [n_div];

    unsigned i,j;
    for ( i = 0; i < n_div; i++)
    {
        raw_data[i]         = new double [n_div];
    }

    for (i = 0; i < n_div; ++i)
    {
        for (j = 0; j < n_div; ++j)
        {
            raw_data[i][j] = data_matrix[i*n_div+j];
        }
    }
    loadFromData(raw_data,n_div,n_div,min,max,min,max);

    for ( i = 0; i < n_div; i++)
    {
        delete [] raw_data[i];
    }

    delete [] raw_data;
    
    setRotation(89,1,0);
    setScale(1,1,1);
    setShift(0.15,0,0);
    setZoom(0.9);

    for (unsigned i=0; i!=coordinates()->axes.size(); ++i)
    {
      coordinates()->axes[i].setMajors(7);
      coordinates()->axes[i].setMinors(4);
    }


    coordinates()->axes[X1].setLabelString("dim 1");
    coordinates()->axes[Y1].setLabelString("dim 2");


    setCoordinateStyle(FRAME);
}
