#ifndef _QT_PLOT_WIDGET_
#define _QT_PLOT_WIDGET_

#include <qcolor.h>
#include <qpainter.h>
#include <qapplication.h>
#include <qframe.h>
#include <QMouseEvent>

#include <qwt_plot.h>
#include <qwt_plot_grid.h>
#include <qwt_plot_curve.h>
#include <qwt_plot_intervalcurve.h>
#include <qwt_symbol.h>
#include <qwt_math.h>

#include <Eigen/Core>
//
//   Array Sizes
//

class QtPlotWidget : public QwtPlot
{
public:
    QtPlotWidget();
    ~QtPlotWidget();

    void set_sample_data(
            Eigen::MatrixXd const&  data,
            Eigen::VectorXd const&  labels
            );
    void set_regression_data(
            Eigen::MatrixXd const& domain,
            Eigen::ArrayXd const& mean,
            Eigen::ArrayXd const& variance
            );

//    void mouseMoveEvent(QMouseEvent *event);
protected:
    virtual void paintEvent(QPaintEvent *);
   // void drawContents(QPainter *p);

private:
    double *m_x_val;
    double *m_y_val;

    double *m_domain_x;
    double *m_mean_val;

    bool m_data_set;
    bool m_inference_data_set;

    QwtPlotCurve m_curve;
    QwtSymbol *m_curve_symbol;
    QwtPlotCurve m_mean_curve;
    QwtSymbol *m_mean_symbol;
    QwtPlotIntervalCurve m_var_curve;
    QwtPlotGrid m_grid;
};

#endif //_QT_PLOT_WIDGET_
