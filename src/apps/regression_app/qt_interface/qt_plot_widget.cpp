#include <apps/regression_app/qt_interface/qt_plot_widget.h>
#include <iostream>
#include <qwt_plot_canvas.h>
#include <qwt_plot_layout.h>

using namespace std;

QtPlotWidget::QtPlotWidget()
    :m_x_val(NULL)
    ,m_y_val(NULL)
    ,m_domain_x(NULL)
    ,m_mean_val(NULL)
    ,m_data_set(false)
    ,m_inference_data_set(false)
{
    //
    //  Frame style
    //

    setFrameStyle(QFrame::NoFrame);
    plotLayout()->setAlignCanvasToScales(true);
    //
    //  define curve styles
    //
    m_curve_symbol = new QwtSymbol(QwtSymbol::Cross, Qt::NoBrush,
                               QPen(Qt::black), QSize(5, 5) );
    m_curve.setSymbol(m_curve_symbol);
    m_curve.setStyle(QwtPlotCurve::NoCurve);
    m_curve.setRenderHint(QwtPlotItem::RenderAntialiased);


    m_mean_symbol = new QwtSymbol(QwtSymbol::NoSymbol, Qt::NoBrush,
                              QPen(Qt::black), QSize(5, 5) );
    m_mean_curve.setSymbol(m_mean_symbol );
    m_mean_curve.setPen(QColor(Qt::blue));
    m_mean_curve.setStyle(QwtPlotCurve::Lines);
    m_mean_curve.setRenderHint(QwtPlotItem::RenderAntialiased);

    // variance curve
    m_var_curve.setStyle(QwtPlotIntervalCurve::Tube);
    m_var_curve.setBrush(QBrush(QColor(0,0,124,50),Qt::SolidPattern));
    m_var_curve.setPen(QPen(Qt::NoPen));
    m_var_curve.setRenderHint(QwtPlotItem::RenderAntialiased);

    setCanvasBackground(QBrush(Qt::white,Qt::SolidPattern));

    m_grid.setPen(QPen(QColor(0,0,0,50), 0.0, Qt::DotLine));
    m_grid.enableX(true);
    m_grid.enableXMin(true);
    m_grid.enableY(true);
    m_grid.enableYMin(false);
    m_grid.attach(this);

}

QtPlotWidget::~QtPlotWidget()
{
    if(m_x_val != NULL)
        delete m_x_val;
    if(m_y_val != NULL)
        delete m_y_val;
    if(m_domain_x != NULL)
        delete m_domain_x;
    if(m_mean_val != NULL)
        delete m_mean_val;
}

/*void QtPlotWidget::mouseMoveEvent(QMouseEvent *event)
{
    cout<<event->x()<<";"<<event->y()<<endl;
}*/

void QtPlotWidget::paintEvent(QPaintEvent *event)
{
    QFrame::paintEvent(event);

    QPainter painter(this);
    painter.setClipRect(contentsRect());
    replot();

}

void QtPlotWidget::set_sample_data(
        Eigen::MatrixXd const&  data,
        Eigen::VectorXd const&  labels
        )
{
    unsigned int size = labels.size();
    if(m_x_val != NULL)
    {
        delete m_x_val;
    }
    if(m_y_val != NULL)
    {
        delete m_y_val;
    }
    m_x_val = new double[size];
    m_y_val = new double[size];

    for(unsigned int i = 0 ; i < size; i++)
    {
        m_x_val[i] = data(i,0);
        m_y_val[i] = labels(i);
    }

    m_curve.setRawSamples(m_x_val, m_y_val, size);

    m_curve.attach(this);
}

void QtPlotWidget::set_regression_data(
        Eigen::MatrixXd const& domain,
        Eigen::ArrayXd const& mean,
        Eigen::ArrayXd const& variance
        )
{
    unsigned int size = mean.size();
    if(m_domain_x != NULL)
    {
        delete m_domain_x;
    }
    if(m_mean_val != NULL)
    {
        delete m_mean_val;
    }
    m_domain_x = new double[size];
    m_mean_val = new double[size];

    QVector<QwtIntervalSample> samples;

    for(unsigned int i = 0 ; i < size; i++)
    {
        m_domain_x[i] = domain(i,0);
        m_mean_val[i] = mean(i);
        samples.append(QwtIntervalSample(domain(i,0),mean(i)-2*sqrt(variance(i)),mean(i)+2*sqrt(variance(i))));
    }

    m_var_curve.setSamples(samples);
    m_mean_curve.setRawSamples(m_domain_x, m_mean_val, size);

    m_mean_curve.attach(this);
    m_var_curve.attach(this);

}
