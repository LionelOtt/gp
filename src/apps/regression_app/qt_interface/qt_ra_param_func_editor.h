#ifndef _QT_RA_GP_OPTIONS_H_
#define _QT_RA_GP_OPTIONS_H_

#include <apps/regression_app/core/regression_app_core.h>
#include <QtGui/QWidget>
#include <string>
#include "ui_qt_ra_param_func_editor.h"

class QtRAInterface;

class QtRAParamFuncEditor
    : public QWidget
{
    Q_OBJECT

    public:
        QtRAParamFuncEditor(QWidget *parent = 0, Qt::WFlags flags = 0);
        ~QtRAParamFuncEditor();

        void create_qt_connections();
        QtRAInterface *m_interface;

    private:
        Ui::QtRAParamFuncEditor ui;

        double m_resolution;
        double m_min;
        double m_max;
};

#endif //_QT_RA_GP_OPTIONS_H_
