#ifndef _QT_RA_DATA_MANAGER_H_
#define _QT_RA_DATA_MANAGER_H_

#include <apps/regression_app/core/regression_app_core.h>
#include <QtGui/QDockWidget>
#include "ui_qt_ra_data_manager.h"

class QtRAInterface;
class QListWidgetItem;

class QtRADataManager
    : public QDockWidget
{
    Q_OBJECT

    public:
        QtRADataManager(QWidget *parent = 0, Qt::WFlags flags = 0);
        ~QtRADataManager();

        void update();
        void create_qt_connections();
        void fill_data_inspection(Eigen::MatrixXd data,Eigen::VectorXd targets);
        QtRAInterface *m_interface;

        bool open_gp_data(std::string path, Eigen::MatrixXd &data, Eigen::VectorXd &targets);
        bool save_gp_data(std::string path, Eigen::MatrixXd const& data, Eigen::VectorXd const& targets);

    signals:
        void update_signal();

    public slots:
        void change_selected_data_point_x(int new_row);
        void change_selected_data_point_y(int new_row);
        void open_file();
        void save_as();
        void save();
        void add_new_data();
        void clear_data();
    private:
            Ui::QtRADataManager ui;
};

#endif //_QT_RA_DATA_MANAGER_H_
