#ifndef _REGRESSION_APP_CORE_H_
#define _REGRESSION_APP_CORE_H_

#include "../regression_app_generic_interface.h"

#include <gp/covariance_functions/squared_exponential.h>
#include <gp/covariance_functions/neural_network.h>
#include <gp/mean_functions/stationary_mean.h>
#include <gp/mean_functions/zero_mean.h>
#include <gp/noise_functions/stationary_noise.h>
#include <gp/gp/gp_simple.h>
#include <gp/gp/gp_large_data.h>
#include <gp/gp/optimisers/nlopt_optimiser.h>

using namespace gp;

class RegressionAppCore
{
    public:
        RegressionAppCore();
        ~RegressionAppCore(){}

        void set_interface(RAGenericInterface* graphicInterface_);
        RAGenericInterface* get_interface();

        void set_cov_hyper_params(std::vector<double> const& params);
        void set_mean_hyper_params(std::vector<double> const& params);
        void set_noise_hyper_params(std::vector<double> const& params);

        std::vector<double> get_params();
        void set_data(
                Eigen::MatrixXd const&  data,
                Eigen::VectorXd const&  labels
        );

        void add_data(
                Eigen::MatrixXd const&  data,
                Eigen::VectorXd const&  labels
        );

        Eigen::MatrixXd get_data();
        Eigen::VectorXd get_labels();

        std::vector<double> get_noise_hyper_params();

        void clear_training_data();

        GPResult inference(Eigen::MatrixXd const& data);

        void learn_params();

        void set_grad_calculation_internal(bool const& policy);
        bool has_derivatives();

        void start();

    private:
        RAGenericInterface *m_graphic_interface;

        GPLargeData m_gp;

        NLoptOptimiser m_nlopt;
};

#endif //_REGRESSION_APP_CORE_H_
