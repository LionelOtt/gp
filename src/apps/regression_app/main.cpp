#include <QtGui/QApplication>
#include "qt_interface/qt_regression_app_interface.h"
#include "core/regression_app_core.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    //RegressionAppCore is independent of graphic interface platform:
    RegressionAppCore core;

    //Qt graphic interface for regression app.
    QtRAInterface interface(&core);

    //Pointer assignment
    core.set_interface(&interface);
    core.start();

    //Show graphic interfce:
    interface.show();
    return app.exec();
}
