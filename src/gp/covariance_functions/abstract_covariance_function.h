#ifndef __GP_ABSTRACT_COVARIANCE_FUNCTION_H__
#define __GP_ABSTRACT_COVARIANCE_FUNCTION_H__

#include <vector>
#include <Eigen/Core>

#include <gp/util/parametric_function.h>

namespace gp
{

/**
 * \brief Generic covariance function.
 *
 * This class represents a generic covariance function. All generated covariance
 * functions must inherit from this covariance function to inherit methods for
 * clamping a specific hyperparameter or remapping them.
 *
 * It first evaluates a clamped parameters and then the remapping.
 */
class AbstractCovarianceFunction
        : public ParametricFunction
{
    public:
        AbstractCovarianceFunction(unsigned int dim = 1);
        virtual ~AbstractCovarianceFunction();

    private:
        /**
         * \brief Computes the covariance matrix computed from two arrays of
         * data.
         *
         * This method is called with the data matrices and parameters
         * and returns the covariance matrix.
         *
         * Matrices are assumed to contain one measurement per row.
         *
         * \param x1 first data matrix
         * \param x2 second data matrix
         * \param params covariance function parameters
         * \return covariance matrix computed from the given data
         */
        virtual Eigen::MatrixXd do_cov(
                Eigen::MatrixXd const&  x1,
                Eigen::MatrixXd const&  x2,
                ParamVec_t const&       params
        ) = 0;

        /**
         * \brief Computes the covariance matrix using the i'th derivative of
         * the cov function.
         *
         * This method is called with the data matrices, parameters and
         * the index of the parameter w.r.t. wich the derivate will be
         * calculated.
         *
         * Matrices are assumed to contain one measurement per row.
         *
         * \param x1 first data matrix
         * \param x2 second data matrix
         * \param params covariance function parameters
         * \param deriv_i index of the param to calculate derivate
         * \return covariance matrix computed from the given data
         */
        virtual Eigen::MatrixXd do_cov_deriv(
                Eigen::MatrixXd const&  x1,
                Eigen::MatrixXd const&  x2,
                ParamVec_t const&       params,
                unsigned int const&     deriv_i
        ) = 0;

    public:
        /**
         * \brief Returns a boolean true if the class has derivative
         * information, false otherwise.
         *
         * \return Returns true if the covariance function has derivative
         * information
         */
        virtual bool has_derivatives() = 0;


        /**
         * \brief Computes the covariance matrix computed from two arrays of
         * data.
         *
         * This method is called with the data matrices and parameters
         * and returns the covariance matrix.
         *
         * Matrices are assumed to contain one measurement per row.
         *
         * \param x1 first data matrix
         * \param x2 second data matrix
         * \param params covariance function parameters
         * \param free_params if the params vector constains only the free
         *                    parameters.
         * \return covariance matrix computed from the given data
         */
        virtual Eigen::MatrixXd cov(
                Eigen::MatrixXd const&  x1,
                Eigen::MatrixXd const&  x2,
                ParamVec_t const&       params,
                bool                   free_params = true
        );

        /**
         * \brief Returns the covariance matrix computed from two arrays of
         * data.
         *
         * \param x1 First matrix where rows are measurements from which to
         * compute the covariance matrix
         * \param x2 Second matrix where rows are measurements from which to
         * compute the covariance matrix
         * \return the covariance matrix computed from the given data
         */
        virtual Eigen::MatrixXd cov(
                Eigen::MatrixXd const& x1,
                Eigen::MatrixXd const& x2
        );

        /**
         * \brief Returns the covariance matrix computed from two arrays of
         * data.
         *
         * \param x1 Matrix where rows are measurements from which to
         * compute the covariance matrix
         * \return the covariance matrix computed from the given data
         */
        virtual Eigen::MatrixXd cov(Eigen::MatrixXd const& x1);

        /**
         * \brief Returns the covariance matrix computed from two arrays of
         * data.
         *
         * \param x1 Matrix where rows are measurements from which to
         * compute the covariance matrix
         * \param params covariance function parameters
         * \param free_params if the params vector constains only the free
         *                    parameters.
         * \return the covariance matrix computed from the given data
         */
        virtual Eigen::MatrixXd cov(
                Eigen::MatrixXd const&  x1,
                ParamVec_t const&       params,
                bool                   free_params = true
        );

        /**
         * \brief Computes the covariance matrix using the i'th derivative of
         * the cov function.
         *
         * This method is called with the data matrices, parameters and
         * the index of the parameter w.r.t. wich the derivate will be
         * calculated.
         *
         * Matrices are assumed to contain one measurement per row.
         *
         * \param x1 first data matrix
         * \param x2 second data matrix
         * \param params covariance function parameters
         * \param deriv_i index of the param to calculate derivate
         * \param free_params if the params vector constains only the free
         *                    parameters.
         * \return covariance matrix computed from the given data
         */
        virtual Eigen::MatrixXd cov_deriv(
                Eigen::MatrixXd const&  x1,
                Eigen::MatrixXd const&  x2,
                ParamVec_t const&       params,
                unsigned int const&     deriv_i,
                bool                   free_params = true
        );

        /**
         * \brief Returns the derivative covariance matrix computed from two
         *        arrays of data.
         *
         * \param x1 First matrix where rows are measurements from which to
         *           compute the covariance matrix
         * \param x2 Second matrix where rows are measurements from which to
         *           compute the covariance matrix
         * \param deriv_i Index of the parameter from wich to calculate the
         *                derivative.
         * \return the covariance matrix computed from the given data
         */
        virtual Eigen::MatrixXd cov_deriv(
                Eigen::MatrixXd const&  x1,
                Eigen::MatrixXd const&  x2,
                unsigned int const&     deriv_i
        );

        /**
         * \brief Checks if the dimensionality of the data is coherent to what
         *        is expected by the covariance function.
         *
         * In case the dimensionality is not expected, an error will be thrown.
         * Virtual function to allow redefinition when necessary (for example
         * in the summing kernel class).
         *
         * \param x Matrix where rows are measurements and columns are
         *          dimensions.
         */
        virtual void dimensionality_check(Eigen::MatrixXd const&  x);

        /**
         * \brief Returns the squared distance between x1 and x2.
         *
         * \param x1 first group of data
         * \param x2 second group of data
         * \return Squared distance
         */
        static Eigen::MatrixXd squared_distance(
                Eigen::MatrixXd const&  x1,
                Eigen::MatrixXd const&  x2
        );

    protected:
        //! Dimenstion of the data to which the covariance function is
        //! compatible with.
        unsigned int m_dim;
};

} /* gp */

#endif /* __GP_ABSTRACT_COVARIANCE_FUNCTION_H__ */
