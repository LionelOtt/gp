#include <iostream>

#include <gp/covariance_functions/neural_network.h>
#include <gp/gp/error.hpp>


namespace gp
{

NeuralNetwork::NeuralNetwork(unsigned int dim)
    :AbstractCovarianceFunction(dim)
{
    reset_params();
}

Eigen::MatrixXd NeuralNetwork::do_cov(
        Eigen::MatrixXd const&  x1,
        Eigen::MatrixXd const&  x2,
        ParamVec_t const&       params
)
{
    Eigen::VectorXd sigma(m_dim);
    for (unsigned int i = 0; i < m_dim; i++)
    {
        sigma(i) = 1/(params[i]*params[i]);
    }
    double sigma_0 = 1/(params[m_dim]*params[m_dim]);

    return params[m_dim+1] *
           params[m_dim+1] *
           asin((2*sigma_0 + 2*(x1*sigma.asDiagonal()*x2.transpose()).array())
           /sqrt(((1 + 2*sigma_0 +
           2*(x1.array().square().matrix()*sigma).array()).matrix()*
           (1 + 2*sigma_0 + 2*(x2.array().square().matrix()
           *sigma).array()).matrix().transpose()).array()));
}

Eigen::MatrixXd NeuralNetwork::do_cov_deriv(
        Eigen::MatrixXd const&  x1,
        Eigen::MatrixXd const&  x2,
        ParamVec_t const&       params,
        unsigned int const&     deriv_i
)
{
    Eigen::VectorXd sigma(m_dim);
    double s2_f = params[m_dim+1]*params[m_dim+1];
    double s2_0 = 1/(params[m_dim]*params[m_dim]);

    for(unsigned int i = 0; i < m_dim; i++)
    {
        sigma(i) = 1/(params[i]*params[i]);
    }


    if (deriv_i < m_dim)
    {
        Eigen::MatrixXd a = (1 + 2*(s2_0 + (x1.array().square().matrix()*sigma)
                                    .array())).matrix() * (1 + 2*(s2_0 +
                                    (x2.array().square().matrix()*
                                    sigma).array())).matrix().transpose();

        return -4*s2_f/(params[deriv_i]*params[deriv_i]*params[deriv_i])*
                sqrt(a.array() -
                4*(s2_0 + (x1*sigma.asDiagonal()*
                x2.transpose()).array()).square()).inverse()*
                ((x1.col(deriv_i)*x2.col(deriv_i).transpose() -
                ((s2_0 + (x1*sigma.asDiagonal()*x2.transpose()).array())/
                a.array()*((1 + 2*(s2_0 + (x1.array().square().matrix()*
                sigma).array())).matrix()*
                x2.col(deriv_i).array().square().matrix().transpose() +
                x1.col(deriv_i).array().square().matrix()*(1 + 2*(s2_0 +
                (x2.array().square().matrix()*
                sigma).array())).transpose().matrix()).array()).matrix()))
                .array();
    }
    else if (deriv_i == m_dim)
    {
        Eigen::MatrixXd a = (1 + 2*(s2_0 + (x1.array().square().matrix()*sigma)
                             .array())).matrix() * (1 + 2*(s2_0 +
                             (x2.array().square().matrix()*
                              sigma).array())).matrix().transpose();
        return -4*s2_f/(params[deriv_i]*params[deriv_i]*params[deriv_i])*
                sqrt(a.array() - 4*(s2_0 + (x1*sigma.asDiagonal()*
                x2.transpose()).array()).square()).inverse()*
                ((1-((s2_0 + (x1*sigma.asDiagonal()*x2.transpose()).array())/
                a.array()*((1 + 2*(s2_0 + (x1.array().square().matrix()
                *sigma).array())).replicate(1,x2.rows())
                + (1 + 2*(s2_0 + (x2.array().square().matrix()
                *sigma).array())).replicate(1,x1.rows()).transpose()))));
    }
    else if (deriv_i == m_dim + 1)
    {
        return 2*params[m_dim+1]*asin((2*s2_0 + 2*(x1*sigma.asDiagonal()*
               x2.transpose()).array())/sqrt(((1 + 2*s2_0 +
               2*(x1.array().square().matrix()*sigma).array()).matrix()*
               (1 + 2*s2_0 + 2*(x2.array().square().matrix()
               *sigma).array()).matrix().transpose()).array()));
    }
    else
    {
        throw DerivativeException("Wrong derivative index requested, no such"
                              "parameter to derive with respect to.");
    }
}

unsigned int NeuralNetwork::get_nr_of_params()
{
    return m_dim + 2;
}

bool NeuralNetwork::has_derivatives()
{
    return true;
}

std::string NeuralNetwork::get_name()
{
    std::string name("Neural Network");
    return name;
}

} /* gp */
