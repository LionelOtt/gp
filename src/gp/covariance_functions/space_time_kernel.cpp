#include <iostream>

#include <gp/covariance_functions/space_time_kernel.h>
#include <gp/gp/error.hpp>

namespace gp
{

SpaceTimeKernel::SpaceTimeKernel(unsigned int dim)
    :AbstractCovarianceFunction(dim)
{

}


Eigen::MatrixXd SpaceTimeKernel::do_cov(
        Eigen::MatrixXd const&  x1,
        Eigen::MatrixXd const&  x2,
        ParamVec_t const&       params
)
{
    //The spatial component of the data is x.block(0,0,x.rows(),x.cols()-1)
    //The temporal component of the data is x.col(x.cols())

    Eigen::MatrixXd result;

    Eigen::MatrixXd result_space;
    Eigen::MatrixXd result_time;

    result_space = m_cov_func_space->cov(x1.block(0,0,x1.rows(),x1.cols()-1),
                                 x2.block(0,0,x2.rows(),x2.cols()-1),
                                 get_hyper_params_for_space(params),false);


    result_time = m_cov_func_time->cov(x1.col(x1.cols()-1),
                                 x2.col(x2.cols()-1),
                                 get_hyper_params_for_time(params),false);

    result = result_space.array()*result_time.array();

    return result;
}

Eigen::MatrixXd SpaceTimeKernel::do_cov_deriv(
        Eigen::MatrixXd const&  x1,
        Eigen::MatrixXd const&  x2,
        ParamVec_t const&       params,
        unsigned int const&     deriv_i
)
{

    //The spatial component of the data is x.block(0,0,x.rows(),x.cols()-1)
    //The temporal component of the data is x.col(x.cols())

    Eigen::MatrixXd result;

    result = m_cov_func_space->cov_deriv(x1.block(0,0,x1.rows(),x1.cols()-1),
                                       x2.block(0,0,x2.rows(),x2.cols()-1),
                                       get_hyper_params_for_space(params),
                                       deriv_i);

    result *= m_cov_func_time->cov_deriv(x1.col(x1.cols()-1),
                                       x2.col(x2.cols()-1),
                                       get_hyper_params_for_time(params),
                                       deriv_i);

    return result;
}

unsigned int SpaceTimeKernel::get_nr_of_params()
{
    unsigned int n_params = 0;

    n_params = m_cov_func_space->get_nr_of_params();
    n_params += m_cov_func_time->get_nr_of_params();

    return n_params;
}

bool SpaceTimeKernel::has_derivatives()
{
    bool has_derivatives = true;

    has_derivatives = has_derivatives && m_cov_func_space->has_derivatives();
    has_derivatives = has_derivatives && m_cov_func_time->has_derivatives();

    return has_derivatives;
}

void SpaceTimeKernel::dimensionality_check(Eigen::MatrixXd const&  x)
{
    AbstractCovarianceFunction::dimensionality_check(x);
    m_cov_func_space->dimensionality_check(x);
    m_cov_func_time->dimensionality_check(x);
}

void SpaceTimeKernel::dimensionality_check_params(ParamVec_t const& params)
{
    AbstractCovarianceFunction::dimensionality_check_params(params);
    m_cov_func_space->dimensionality_check_params(
                                        get_hyper_params_for_space(params));
    m_cov_func_time->dimensionality_check_params(
                                         get_hyper_params_for_time(params));
}

Eigen::MatrixXd SpaceTimeKernel::cov(
        Eigen::MatrixXd const&  x1,
        Eigen::MatrixXd const&  x2,
        ParamVec_t const&       params,
        bool                   free_params
)
{
    ParamVec_t full_params = params;
    if(free_params)
    {
        full_params.clear();
        ParamVec_t space_params =
                m_cov_func_space->get_hyper_params_from_free_params(
                    get_free_hyper_params_for_space(params));
        ParamVec_t time_params =
                m_cov_func_time->get_hyper_params_from_free_params(
                    get_free_hyper_params_for_time(params));

        full_params.insert(full_params.end(),
                           space_params.begin(),
                           space_params.end());

        full_params.insert(full_params.end(),
                           time_params.begin(),
                           time_params.end());
    }
    return do_cov(x1,x2,full_params);
}

Eigen::MatrixXd SpaceTimeKernel::cov(
        Eigen::MatrixXd const& x1,
        Eigen::MatrixXd const& x2
)
{
    return do_cov(x1,x2,get_hyper_params());
}

Eigen::MatrixXd SpaceTimeKernel::cov(Eigen::MatrixXd const& x1)
{
    return do_cov(x1,x1,get_hyper_params());
}

Eigen::MatrixXd SpaceTimeKernel::cov(
        Eigen::MatrixXd const&  x1,
        ParamVec_t const&       params,
        bool                   free_params
)
{
    return cov(x1,x1,params,free_params);
}

Eigen::MatrixXd SpaceTimeKernel::cov_deriv(
        Eigen::MatrixXd const&  x1,
        Eigen::MatrixXd const&  x2,
        ParamVec_t const&       params,
        unsigned int const&     deriv_i,
        bool                   free_params
)
{
    ParamVec_t full_params = params;
    if(free_params)
    {
        full_params.clear();
        full_params.clear();
        ParamVec_t space_params =
                m_cov_func_space->get_hyper_params_from_free_params(
                    get_free_hyper_params_for_space(params));
        ParamVec_t time_params =
                m_cov_func_time->get_hyper_params_from_free_params(
                    get_free_hyper_params_for_time(params));

        full_params.insert(full_params.end(),
                           space_params.begin(),
                           space_params.end());

        full_params.insert(full_params.end(),
                           time_params.begin(),
                           time_params.end());
    }
    return do_cov_deriv(x1,x2,full_params,deriv_i);
}

Eigen::MatrixXd SpaceTimeKernel::cov_deriv(
        Eigen::MatrixXd const&  x1,
        Eigen::MatrixXd const&  x2,
        unsigned int const&     deriv_i
)
{
    return do_cov_deriv(x1,x2,get_hyper_params(),deriv_i);
}

void SpaceTimeKernel::set_hyper_params(
                              ParamVec_t const& params,
                              bool force
)
{
    dimensionality_check_params(params);
    m_cov_func_space->set_hyper_params(get_hyper_params_for_space(params),force);
    m_cov_func_time->set_hyper_params(get_hyper_params_for_time(params),force);
}

void SpaceTimeKernel::set_free_hyper_params(ParamVec_t const& params)
{
    m_cov_func_space->set_free_hyper_params(
                        get_free_hyper_params_for_space(params));
    m_cov_func_time->set_free_hyper_params(
                        get_free_hyper_params_for_time(params));
}

AbstractCovarianceFunction::ParamVec_t SpaceTimeKernel::get_hyper_params() const
{
    ParamVec_t all_params;

    ParamVec_t space_params = m_cov_func_space->get_hyper_params();
    ParamVec_t time_params = m_cov_func_time->get_hyper_params();

    all_params.insert(all_params.end(),space_params.begin(),space_params.end());
    all_params.insert(all_params.end(),time_params.begin(),time_params.end());

    return all_params;
}

AbstractCovarianceFunction::ParamVec_t SpaceTimeKernel::get_free_hyper_params()
{
    ParamVec_t all_free_params;

    ParamVec_t space_params = m_cov_func_space->get_free_hyper_params();
    ParamVec_t time_params = m_cov_func_time->get_free_hyper_params();

    all_free_params.insert(all_free_params.end(),
                           space_params.begin(),
                           space_params.end());
    all_free_params.insert(all_free_params.end(),
                           time_params.begin(),
                           time_params.end());

    return all_free_params;
}

unsigned int SpaceTimeKernel::get_nr_of_free_params()
{
    unsigned int n_free_params = 0;
    n_free_params += m_cov_func_space->get_nr_of_free_params();
    n_free_params += m_cov_func_time->get_nr_of_free_params();
    return n_free_params;
}

AbstractCovarianceFunction::ParamVec_t
SpaceTimeKernel::get_hyper_params_for_space(ParamVec_t const& params)
{
    //Check if params is coherent with the number of expected parameters.
    unsigned int total_parameters = 0;
    total_parameters += m_cov_func_space->get_nr_of_params();
    total_parameters += m_cov_func_time->get_nr_of_params();
    if(total_parameters != params.size())
    {
        throw ParameterException("SpaceTimeKernel: Number of provided parameters is"
                             " not consistent with members.");
    }

    //Starting index for spatial covariance function is always 0
    unsigned int starting_index = 0;
    //Ending index is always the number of parameters in the spatial component
    unsigned int ending_index = m_cov_func_space->get_hyper_params().size();

    //First check the starting index for the covariance function with index.
    ParamVec_t::const_iterator first = params.begin()+starting_index;
    ParamVec_t::const_iterator last = params.begin()+ending_index;
    ParamVec_t sub_vector(first,last);
    return sub_vector;
}

AbstractCovarianceFunction::ParamVec_t
SpaceTimeKernel::get_hyper_params_for_time(ParamVec_t const& params)
{
    //Check if params is coherent with the number of expected parameters.
    unsigned int total_parameters = 0;
    total_parameters += m_cov_func_space->get_nr_of_params();
    total_parameters += m_cov_func_time->get_nr_of_params();
    if(total_parameters != params.size())
    {
        throw ParameterException("SpaceTimeKernel: Number of provided parameters is"
                             " not consistent with members.");
    }

    //Starting index for time covariance function is always the number
    //of parameters in the spatial one.
    unsigned int starting_index = m_cov_func_space->get_hyper_params().size();
    //Ending index is always the number of parameters in the spatial component
    unsigned int ending_index = starting_index +
                                m_cov_func_time->get_hyper_params().size();

    //First check the starting index for the covariance function with index.
    ParamVec_t::const_iterator first = params.begin()+starting_index;
    ParamVec_t::const_iterator last = params.begin()+ending_index;
    ParamVec_t sub_vector(first,last);
    return sub_vector;
}

AbstractCovarianceFunction::ParamVec_t
SpaceTimeKernel::get_free_hyper_params_for_space(ParamVec_t const& params)
{
    //Check if params is coherent with the number of expected parameters.
    unsigned int total_parameters = 0;
    total_parameters += m_cov_func_space->get_nr_of_free_params();
    total_parameters += m_cov_func_time->get_nr_of_free_params();
    if(total_parameters != params.size())
    {
        throw ParameterException("SpaceTimeKernel: Number of provided parameters is"
                             " not consistent with members.");
    }

    //Starting index for spatial covariance function is always 0
    unsigned int starting_index = 0;
    //Ending index is always the number of parameters in the spatial component
    unsigned int ending_index = starting_index +
                                m_cov_func_space->get_free_hyper_params().size();

    //First check the starting index for the covariance function with index.
    ParamVec_t::const_iterator first = params.begin()+starting_index;
    ParamVec_t::const_iterator last = params.begin()+ending_index;
    ParamVec_t sub_vector(first,last);
    return sub_vector;
}

AbstractCovarianceFunction::ParamVec_t
SpaceTimeKernel::get_free_hyper_params_for_time(ParamVec_t const& params)
{
    //Check if params is coherent with the number of expected parameters.
    unsigned int total_parameters = 0;
    total_parameters += m_cov_func_space->get_nr_of_free_params();
    total_parameters += m_cov_func_time->get_nr_of_free_params();
    if(total_parameters != params.size())
    {
        throw ParameterException("SpaceTimeKernel: Number of provided parameters is"
                             " not consistent with members.");
    }

    //Starting index for time covariance function is always the number
    //of parameters in the spatial one.
    unsigned int starting_index =m_cov_func_space->get_free_hyper_params().size();
    //Ending index is always the number of parameters in the spatial component
    unsigned int ending_index = starting_index +
                                m_cov_func_time->get_free_hyper_params().size();

    //First check the starting index for the covariance function with index.
    ParamVec_t::const_iterator first = params.begin()+starting_index;
    ParamVec_t::const_iterator last = params.begin()+ending_index;
    ParamVec_t sub_vector(first,last);
    return sub_vector;
}

void SpaceTimeKernel::set_space_cov_func(
        boost::shared_ptr<AbstractCovarianceFunction> cov_func)
{
    m_cov_func_space = cov_func;
}

void SpaceTimeKernel::set_time_cov_func(
        boost::shared_ptr<AbstractCovarianceFunction> cov_func)
{
    m_cov_func_time = cov_func;
}

void SpaceTimeKernel::reset_params()
{
    if(m_cov_func_space)
    {
        m_cov_func_space->reset_params();
    }
    if(m_cov_func_time)
    {
        m_cov_func_time->reset_params();
    }
}

void SpaceTimeKernel::fix_param(unsigned int index, double value)
{
    throw BaseException("SpaceTimeKernel: Fix param cant be used for summing kernel"
                    ", try fixing parameters of each covariance function.");
}

void SpaceTimeKernel::set_remapping(std::vector<unsigned int> remapping_vector)
{
    throw BaseException("SpaceTimeKernel: set remapping can't be used for summing"
              " kernel, try remapping parameters of each covariance function.");
}

std::string SpaceTimeKernel::get_name()
{
    std::string name("Space-Time Kernel");
    return name;
}

} /* gp */
