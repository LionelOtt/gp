#include <iostream>

#include <gp/covariance_functions/periodic_exponential.h>
#include <gp/gp/error.hpp>

namespace gp
{

PeriodicExponential::PeriodicExponential(unsigned int dim)
    :AbstractCovarianceFunction(dim)
{
    reset_params();
}

Eigen::MatrixXd PeriodicExponential::do_cov(
        Eigen::MatrixXd const&          x1,
        Eigen::MatrixXd const&          x2,
        ParamVec_t const& params
)
{
    //Check if smoothness parameter is different than zero to avoid
    //indefinitions
    if(params[0] == 0)
    {
        throw ParameterException("PeriodicExponential: Parameter index 0 cannot"
                             "be cero.");
    }

    //Check if period parameter is different than zero to avoid indefinitions
    if(params[1] == 0)
    {
        throw ParameterException("PeriodicExponential: Parameter index 1"
                             " (period) cannot be cero.");
    }

    Eigen::MatrixXd sin_mat =
            ((M_PI*squared_distance(x1, x2).cwiseSqrt().array())
                     /params[1]).array().sin();
    Eigen::MatrixXd z = -2*sin_mat.array().pow(2)/params[0];

    return params[2] * params[2] * exp(z.array());
}

Eigen::MatrixXd PeriodicExponential::do_cov_deriv(
        Eigen::MatrixXd const&  x1,
        Eigen::MatrixXd const&  x2,
        ParamVec_t const&       params,
        unsigned int const&     deriv_i
)
{
    // TODO: Calculate the expresion of the derivative and implement
    // this function.

    if(!has_derivatives())
    {
        throw DerivativeException("Derivative not implemented");
    }

    return  Eigen::MatrixXd();
}

unsigned int PeriodicExponential::get_nr_of_params()
{
    return 3;
}

bool PeriodicExponential::has_derivatives()
{
    return false;
}

std::string PeriodicExponential::get_name()
{
    std::string name("Periodic Exponential");
    return name;
}

} /* gp */
