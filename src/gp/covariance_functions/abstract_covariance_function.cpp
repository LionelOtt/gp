#include <iostream>

#include <gp/covariance_functions/abstract_covariance_function.h>
#include <gp/gp/error.hpp>

namespace gp
{

AbstractCovarianceFunction::AbstractCovarianceFunction(unsigned int dim)
    :m_dim(dim)
{

}

AbstractCovarianceFunction::~AbstractCovarianceFunction()
{

}

Eigen::MatrixXd AbstractCovarianceFunction::cov(
        Eigen::MatrixXd const&          x1,
        Eigen::MatrixXd const&          x2,
        ParamVec_t const&               params,
        bool                            free_params
)
{
    //Check if x1 contains at least one observation
    if(x1.rows() == 0 || x2.rows() == 0)
    {
        throw BaseException("Cannot call covariance on an empty matrix.");
    }
    ParamVec_t full_params = params;
    // If the caller specifies that the parameters are only
    // the free parameters, then the covariance function
    // transforms this parameters to the full param dimensionality.
    if(free_params)
    {
       full_params = get_hyper_params_from_free_params(params);
    }
    dimensionality_check(x1);
    dimensionality_check(x2);
    dimensionality_check_params(full_params);
    return do_cov(x1,x2,full_params);
}

Eigen::MatrixXd AbstractCovarianceFunction::cov(
        Eigen::MatrixXd const& x1,
        Eigen::MatrixXd const& x2
)
{
    return cov(x1, x2, m_params, false);
}

Eigen::MatrixXd AbstractCovarianceFunction::cov(
        Eigen::MatrixXd const&  x1,
        ParamVec_t const&       params,
        bool                    free_params
)
{
    return cov(x1, x1, params, free_params);
}

Eigen::MatrixXd AbstractCovarianceFunction::cov(
        Eigen::MatrixXd const& x1
)
{
    return cov(x1, x1, m_params, false);
}


Eigen::MatrixXd AbstractCovarianceFunction::cov_deriv(
        Eigen::MatrixXd const&  x1,
        Eigen::MatrixXd const&  x2,
        ParamVec_t const&       params,
        unsigned int const&     deriv_i,
        bool                    free_params
)
{
    ParamVec_t full_params = params;
    // If the caller specifies that the parameters are only
    // the free parameters, then the covariance function
    // transforms this parameters to the full param dimensionality.
    if(free_params)
    {
       full_params = get_hyper_params_from_free_params(params);
    }
    dimensionality_check(x1);
    dimensionality_check(x2);
    dimensionality_check_params(full_params);
    return do_cov_deriv(x1,x2,full_params,deriv_i);
}

Eigen::MatrixXd AbstractCovarianceFunction::cov_deriv(
        Eigen::MatrixXd const&          x1,
        Eigen::MatrixXd const&          x2,
        unsigned int const&             deriv_i
)
{
    return cov_deriv(x1,x2,m_params,deriv_i,false);
}

void AbstractCovarianceFunction::dimensionality_check(Eigen::MatrixXd const&  x)
{
    unsigned int input_dims = x.cols();
    if(input_dims != m_dim)
    {
        std::stringstream error;
        error << "Dimensionality mismach: "
              << "Covariance Funcion expected "
              << m_dim
              << " but data has dimensionality "
              << input_dims;
        throw DimensionException(error.str());
    }
}

Eigen::MatrixXd AbstractCovarianceFunction::squared_distance(
        Eigen::MatrixXd const&          x1,
        Eigen::MatrixXd const&          x2
)
{
    // Number of inputs
    unsigned int numx1 = x1.rows();
    unsigned int numx2 = x2.rows();

    // Calculate the mean and remove from data points to increase numerical
    // stability
    Eigen::MatrixXd mu = (double)numx2/(numx1*(numx1+numx2)) *
                                 x1.colwise().sum() +
                         (double)numx1/(numx2*(numx1+numx2)) *
                                 x2.colwise().sum();
    Eigen::MatrixXd x1c = x1 - mu.replicate(numx1,1);
    Eigen::MatrixXd x2c = x2 - mu.replicate(numx2,1);

    return ((x1c.array()*x1c.array())
            .rowwise().sum().replicate(1,numx2) +
           ((x2c.array()*x2c.array()).rowwise()
            .sum()).transpose().replicate(numx1,1) -
            2.0*(x1c*x2c.transpose()).array()).array()
            .max(Eigen::ArrayXXd::Zero(numx1,numx2));
}

} /* gp */
