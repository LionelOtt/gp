#include <iostream>

#include <gp/covariance_functions/summing_kernel.h>
#include <gp/gp/error.hpp>

namespace gp
{

SummingKernel::SummingKernel(unsigned int dim)
    :AbstractCovarianceFunction(dim)
{
    reset_params();
}


Eigen::MatrixXd SummingKernel::do_cov(
        Eigen::MatrixXd const&  x1,
        Eigen::MatrixXd const&  x2,
        ParamVec_t const&       params
)
{
    Eigen::MatrixXd result;
    for(unsigned int i = 0; i < cov_funcs.size(); i++)
    {
        ParamVec_t this_cov_params = get_hyper_params_for_index(i,params);
        if(i == 0)
        {
            result = cov_funcs[i]->cov(x1,x2,this_cov_params,false);
        }
        else
        {
            result += cov_funcs[i]->cov(x1,x2,this_cov_params,false);
        }
    }
    return result;
}

Eigen::MatrixXd SummingKernel::do_cov_deriv(
        Eigen::MatrixXd const&  x1,
        Eigen::MatrixXd const&  x2,
        ParamVec_t const&       params,
        unsigned int const&     deriv_i
)
{
    Eigen::MatrixXd result;
    for(unsigned int i = 0; i < cov_funcs.size(); i++)
    {
        ParamVec_t this_cov_params = get_hyper_params_for_index(i,params);
        if(i == 0)
        {
            result = cov_funcs[i]->cov_deriv(x1,x2,this_cov_params,deriv_i);
        }
        else
        {
            result += cov_funcs[i]->cov(x1,x2,this_cov_params,deriv_i);
        }
    }
    return result;
}

unsigned int SummingKernel::get_nr_of_params()
{
    unsigned int n_params = 0;
    for(unsigned int i = 0; i < cov_funcs.size(); i++)
    {
        n_params += cov_funcs[i]->get_nr_of_params();
    }
    return n_params;
}

bool SummingKernel::has_derivatives()
{
    bool has_derivatives = true;
    for(unsigned int i = 0; i < cov_funcs.size(); i++)
    {
        if(!cov_funcs[i]->has_derivatives())
        {
            has_derivatives = false;
            break;
        }
    }
    return has_derivatives;
}

void SummingKernel::dimensionality_check(Eigen::MatrixXd const&  x)
{
    AbstractCovarianceFunction::dimensionality_check(x);
    for(unsigned int i = 0; i < cov_funcs.size(); i++)
    {
        cov_funcs[i]->dimensionality_check(x);
    }
}

void SummingKernel::dimensionality_check_params(ParamVec_t const& params)
{
    AbstractCovarianceFunction::dimensionality_check_params(params);
    for(unsigned int i = 0; i < cov_funcs.size(); i++)
    {
        ParamVec_t this_cov_params = get_hyper_params_for_index(i,params);
        cov_funcs[i]->dimensionality_check_params(this_cov_params);
    }
}

Eigen::MatrixXd SummingKernel::cov(
        Eigen::MatrixXd const&  x1,
        Eigen::MatrixXd const&  x2,
        ParamVec_t const&       params,
        bool                   free_params
)
{
    ParamVec_t full_params = params;
    if(free_params)
    {
        full_params.clear();
        for(unsigned int i = 0; i < cov_funcs.size(); i++)
        {
            ParamVec_t this_cov_func_params =
                    cov_funcs[i]->get_hyper_params_from_free_params(
                        get_free_hyper_params_for_index(i,params));
            full_params.insert(full_params.end(),
                               this_cov_func_params.begin(),
                               this_cov_func_params.end());
        }
    }
    return do_cov(x1,x2,full_params);
}

Eigen::MatrixXd SummingKernel::cov(
        Eigen::MatrixXd const& x1,
        Eigen::MatrixXd const& x2
)
{
    return do_cov(x1,x2,get_hyper_params());
}

Eigen::MatrixXd SummingKernel::cov(Eigen::MatrixXd const& x1)
{
    return do_cov(x1,x1,get_hyper_params());
}

Eigen::MatrixXd SummingKernel::cov(
        Eigen::MatrixXd const&  x1,
        ParamVec_t const&       params,
        bool                   free_params
)
{
    return cov(x1,x1,params,free_params);
}

Eigen::MatrixXd SummingKernel::cov_deriv(
        Eigen::MatrixXd const&  x1,
        Eigen::MatrixXd const&  x2,
        ParamVec_t const&       params,
        unsigned int const&     deriv_i,
        bool                   free_params
)
{
    ParamVec_t full_params = params;
    if(free_params)
    {
        full_params.clear();
        for(unsigned int i = 0; i < cov_funcs.size(); i++)
        {
            ParamVec_t this_cov_func_params =
                    cov_funcs[i]->get_hyper_params_from_free_params(
                        get_free_hyper_params_for_index(i,params));
            full_params.insert(full_params.end(),
                               this_cov_func_params.begin(),
                               this_cov_func_params.end());
        }
    }
    return do_cov_deriv(x1,x2,full_params,deriv_i);
}

Eigen::MatrixXd SummingKernel::cov_deriv(
        Eigen::MatrixXd const&  x1,
        Eigen::MatrixXd const&  x2,
        unsigned int const&     deriv_i
)
{
    return do_cov_deriv(x1,x2,get_hyper_params(),deriv_i);
}

void SummingKernel::set_hyper_params(
                              ParamVec_t const& params,
                              bool force
)
{
    dimensionality_check_params(params);
    for(unsigned int i = 0; i < cov_funcs.size(); i++)
    {
        ParamVec_t this_cov_params = get_hyper_params_for_index(i,params);
        cov_funcs[i]->set_hyper_params(this_cov_params,force);
    }
}

void SummingKernel::set_free_hyper_params(ParamVec_t const& params)
{
    for(unsigned int i = 0; i < cov_funcs.size(); i++)
    {
        ParamVec_t free_cov_params = get_free_hyper_params_for_index(i,params);
        cov_funcs[i]->set_free_hyper_params(free_cov_params);
    }
}

AbstractCovarianceFunction::ParamVec_t SummingKernel::get_hyper_params() const
{
    ParamVec_t all_params;
    for(unsigned int i = 0; i < cov_funcs.size(); i++)
    {
        ParamVec_t this_cov_func_params = cov_funcs[i]->get_hyper_params();
        all_params.insert(all_params.end(), this_cov_func_params.begin(),
                                            this_cov_func_params.end());
    }
    return all_params;
}

AbstractCovarianceFunction::ParamVec_t SummingKernel::get_free_hyper_params()
{
    ParamVec_t all_free_params;
    for(unsigned int i = 0; i < cov_funcs.size(); i++)
    {
        ParamVec_t this_cov_func_params = cov_funcs[i]->get_free_hyper_params();
        all_free_params.insert(all_free_params.end(),
                               this_cov_func_params.begin(),
                               this_cov_func_params.end());
    }
    return all_free_params;
}

unsigned int SummingKernel::get_nr_of_free_params()
{
    unsigned int n_free_params = 0;
    for(unsigned int i = 0; i < cov_funcs.size(); i++)
    {
        n_free_params += cov_funcs[i]->get_nr_of_free_params();
    }
    return n_free_params;
}

AbstractCovarianceFunction::ParamVec_t
SummingKernel::get_free_hyper_params_for_index(unsigned int const& index,
                                               ParamVec_t const& params)
{
    //Check if index is coherent with the number of covariance functions.
    if(index >= cov_funcs.size())
    {
        throw DimensionException("SummingKernel: Index of covariance function "
                             "does not exist.");
    }
    //Check if params is coherent with the number of expected parameters.
    unsigned int total_parameters = 0;
    for(unsigned int i = 0; i < cov_funcs.size(); i++)
    {
        total_parameters += cov_funcs[i]->get_nr_of_free_params();
    }
    if(total_parameters != params.size())
    {
        throw ParameterException("SummingKernel: Number of provided parameters is"
                             " not consistent with members.");
    }

    unsigned int starting_index = 0;
    for(unsigned int i = 0; i < index; i++)
    {
        starting_index += cov_funcs[i]->get_nr_of_free_params();
    }

    unsigned int ending_index = starting_index +
                                cov_funcs[index]->get_nr_of_free_params();

    //First check the starting index for the covariance function with index.
    ParamVec_t::const_iterator first = params.begin()+starting_index;
    ParamVec_t::const_iterator last = params.begin()+ending_index;
    ParamVec_t sub_vector(first,last);
    return sub_vector;
}

AbstractCovarianceFunction::ParamVec_t
SummingKernel::get_hyper_params_for_index(unsigned int const& index,
                                          ParamVec_t const& params)
{
    //Check if index is coherent with the number of covariance functions.
    if(index >= cov_funcs.size())
    {
        throw DimensionException("SummingKernel: Index of covariance function "
                             "does not exist.");
    }

    //Check if params is coherent with the number of expected parameters.
    unsigned int total_parameters = 0;
    for(unsigned int i = 0; i < cov_funcs.size(); i++)
    {
        total_parameters += cov_funcs[i]->get_hyper_params().size();
    }
    if(total_parameters != params.size())
    {
        throw ParameterException("SummingKernel: Number of provided parameters is"
                             " not consistent with members.");
    }

    unsigned int starting_index = 0;
    for(unsigned int i = 0; i < index; i++)
    {
        starting_index += cov_funcs[i]->get_hyper_params().size();
    }

    unsigned int ending_index = starting_index +
                                cov_funcs[index]->get_hyper_params().size();

    //First check the starting index for the covariance function with index.
    ParamVec_t::const_iterator first = params.begin()+starting_index;
    ParamVec_t::const_iterator last = params.begin()+ending_index;
    ParamVec_t sub_vector(first,last);
    return sub_vector;
}

void SummingKernel::add_cov_func(
        boost::shared_ptr<AbstractCovarianceFunction> cov_func)
{
    cov_funcs.push_back(cov_func);
}


void SummingKernel::reset_params()
{
    for(unsigned int i = 0; i < cov_funcs.size(); i++)
    {
        cov_funcs[i]->reset_params();
    }
}

void SummingKernel::fix_param(unsigned int index, double value)
{
    throw BaseException("SummingKernel: Fix param cant be used for summing kernel"
                    ", try fixing parameters of each covariance function.");
}

void SummingKernel::set_remapping(std::vector<unsigned int> remapping_vector)
{
    throw BaseException("SummingKernel: set remapping can't be used for summing"
              " kernel, try remapping parameters of each covariance function.");
}

AbstractCovarianceFunction::ParamVec_t
     SummingKernel::get_hyper_params_from_free_params(ParamVec_t free_params)
{
    ParamVec_t vector;
    for(unsigned int i = 0; i < cov_funcs.size(); i++)
    {
        ParamVec_t free_hyper_params_i = get_free_hyper_params_for_index(i, free_params);
        ParamVec_t cov_func_i_vector = cov_funcs[i]->get_hyper_params_from_free_params(free_hyper_params_i);
        vector.insert(vector.end(),
                           cov_func_i_vector.begin(),
                           cov_func_i_vector.end());
    }
    return vector;
}

std::string SummingKernel::get_name()
{
    std::string name("Summing Kernel");
    return name;
}



} /* gp */
