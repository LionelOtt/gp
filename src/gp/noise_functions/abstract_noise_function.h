#ifndef __GP_ABSTRACT_NOISE_FUNCTION_H__
#define __GP_ABSTRACT_NOISE_FUNCTION_H__

#include <Eigen/Core>

#include <gp/util/parametric_function.h>

namespace gp
{

class AbstractNoiseFunction
        :public ParametricFunction
{
    public:
        //! Typedef for the storage of parameters
        typedef std::vector<double> ParamVec_t;

    public:
        AbstractNoiseFunction(unsigned int dim = 1);

        virtual ~AbstractNoiseFunction();

        /**
         * \brief Checks if the dimensionality of the data is coherent to what
         *        is expected by the covariance function.
         *
         * In case the dimensionality is not expected, an error will be thrown.
         * Virtual function to allow redefinition when necessary (for example
         * in the summing kernel class).
         *
         * \param x Matrix where rows are measurements and columns are
         *          dimensions.
         */
        virtual void dimensionality_check(Eigen::MatrixXd const&  x);

        /**
         * \brief Returns a boolean true if the class has derivative
         * information, false otherwise.
         *
         * \return Returns true if the mean function has derivative
         * information
         */
        virtual bool has_derivatives() = 0;

    private:
        virtual Eigen::VectorXd do_noise(Eigen::MatrixXd const& data,
                                        ParamVec_t const&      params) = 0;

    public:
        Eigen::VectorXd noise(Eigen::MatrixXd const& data,
                             ParamVec_t const&      params,
                             bool                   free_params = true);

        Eigen::VectorXd noise(Eigen::MatrixXd const& data);

    protected:
        //! Dimenstion of the data to which the noise function is
        //! compatible with.
        unsigned int m_dim;
};
    
} /* gp */

#endif /* __GP_ABSTRACT_NOISE_FUNCTION_H__ */
