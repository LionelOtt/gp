#include <gp/noise_functions/stationary_noise.h>
namespace gp
{

StationaryNoise::StationaryNoise(unsigned int dim)
    :AbstractNoiseFunction(dim)
{
    reset_params();
}

Eigen::VectorXd StationaryNoise::do_noise(Eigen::MatrixXd const& data,
                                         ParamVec_t const&      params)
{
    //noise is the noise std
    double noise = params[0];
    return Eigen::VectorXd::Ones(1)*noise;
}

unsigned int StationaryNoise::get_nr_of_params()
{
    return 1;
}

std::string StationaryNoise::get_name()
{
    return "Stationary Noise";
}

bool StationaryNoise::has_derivatives()
{
    return false;
}

} /* gp */
