#include <gp/noise_functions/abstract_noise_function.h>
#include <gp/gp/error.hpp>

namespace gp
{

AbstractNoiseFunction::AbstractNoiseFunction(unsigned int dim)
    :m_dim(dim)
{

}

AbstractNoiseFunction::~AbstractNoiseFunction()
{

}

Eigen::VectorXd AbstractNoiseFunction::noise(Eigen::MatrixXd const& data,
                                           ParamVec_t const&      params,
                                           bool                   free_params)
{
    ParamVec_t full_params = params;
    // If the caller specifies that the parameters are only
    // the free parameters, then the covariance function
    // transforms this parameters to the full param dimensionality.
    if(free_params)
    {
       full_params = get_hyper_params_from_free_params(params);
    }
    dimensionality_check(data);
    dimensionality_check_params(full_params);
    return do_noise(data,full_params);
}

Eigen::VectorXd AbstractNoiseFunction::noise(Eigen::MatrixXd const& data)
{
    return noise(data,m_params, false);
}

void AbstractNoiseFunction::dimensionality_check(Eigen::MatrixXd const&  x)
{
    unsigned int input_dims = x.cols();
    if(input_dims != m_dim)
    {
        std::stringstream error;
        error << "Dimensionality mismach: "
              << "Noise Funcion expected "
              << m_dim
              << " but data has dimensionality "
              << input_dims;
        throw DimensionException(error.str());
    }
}

} /* gp */
