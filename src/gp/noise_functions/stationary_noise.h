#ifndef __GP_STATIONARY_NOISE_H__
#define __GP_STATIONARY_NOISE_H__

#include <gp/noise_functions/abstract_noise_function.h>

namespace gp
{

/**
 * \brief Stationary noise function. One value for noise std among all domain.
 *
 * It is assumed that the target values are noisy, where the noise
 * comes from N(0, s), a Gaussian with zero mean and sigma. The sigma
 * paramerter is set using this method.
 */

class StationaryNoise
    :public AbstractNoiseFunction
{
    public:
        StationaryNoise(unsigned int dim = 1);

        Eigen::VectorXd do_noise(Eigen::MatrixXd const& data,
                                ParamVec_t const&      params);

        unsigned int get_nr_of_params();

        std::string get_name();

        bool has_derivatives();
};
    
} /* gp */

#endif /* __GP_STATIONARY_NOISE_H__ */
