#include <gp/mean_functions/zero_mean.h>

namespace gp
{

Eigen::VectorXd ZeroMean::do_mean(Eigen::MatrixXd const& data,
                                  ParamVec_t const&      params)
{
    return Eigen::VectorXd::Zero(data.rows());
}

unsigned int ZeroMean::get_nr_of_params()
{
    return 0;
}

std::string ZeroMean::get_name()
{
    return "Zero Mean";
}

bool ZeroMean::has_derivatives()
{
    return false;
}

} /* gp */
