#include <gp/mean_functions/stationary_mean.h>
namespace gp
{

StationaryMean::StationaryMean(unsigned int dim)
    :AbstractMeanFunction(dim)
{
    reset_params();
}

Eigen::VectorXd StationaryMean::do_mean(Eigen::MatrixXd const& data,
                                        ParamVec_t const&      params)
{
    double mean = params[0];
    return Eigen::VectorXd::Ones(data.rows())*mean;
}

unsigned int StationaryMean::get_nr_of_params()
{
    return 1;
}

std::string StationaryMean::get_name()
{
    return "Stationary Mean";
}

bool StationaryMean::has_derivatives()
{
    return false;
}

} /* gp */
