#ifndef __GP_STATIONARY_MEAN_H__
#define __GP_STATIONARY_MEAN_H__

#include <gp/mean_functions/abstract_mean_function.h>

namespace gp
{

class StationaryMean
    :public AbstractMeanFunction
{
    public:
        StationaryMean(unsigned int dim = 1);

        Eigen::VectorXd do_mean(Eigen::MatrixXd const& data,
                                ParamVec_t const&      params);

        unsigned int get_nr_of_params();

        std::string get_name();

        bool has_derivatives();
};
    
} /* gp */

#endif /* __GP_ZERO_MEAN_H__ */
