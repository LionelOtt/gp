#include <gtest/gtest.h>

#include <gp/covariance_functions/squared_exponential.h>
#include <gp/tests/test_data.hpp>
#include <gp/tests/test_helpers.hpp>


using namespace gp;


TEST(SquaredExponential, OneDim)
{
    SquaredExponential covfun(1);

    std::vector<double> hyper_params;
    hyper_params.push_back(0.5);
    hyper_params.push_back(1.5);
    covfun.set_hyper_params(hyper_params);

    Eigen::MatrixXd result(4, 4);
    result <<
                2.250000000000000e+00,     9.689747833645054e-02,     8.074866852906979e-08,     2.519696233876082e-12,
                9.689747833645054e-02,     2.250000000000000e+00,     8.299058836650865e-03,     9.016971123712967e-22,
                8.074866852906979e-08,     8.299058836650865e-03,     2.250000000000000e+00,     1.233216442316144e-38,
                2.519696233876082e-12,     9.016971123712967e-22,     1.233216442316144e-38,     2.250000000000000e+00;

    compare_matrices(covfun.cov(data_4_1()), result);
}

TEST(SquaredExponential, TwoDim)
{
    SquaredExponential covfun(2);

    std::vector<double> hyper_params;
    hyper_params.push_back(0.5);
    hyper_params.push_back(0.75);
    hyper_params.push_back(1.5);
    covfun.set_hyper_params(hyper_params);

    Eigen::MatrixXd result(4, 6);
    result <<
              7.752015081229504e-28,     2.856244747446911e-51,     7.502597514965376e-14,     1.095406718862530e-27,     4.933019715235868e-25,     1.384680393594659e-11,
              9.038190899469565e-04,     3.857007810027780e-16,     4.396182967326130e-37,     1.148225178919909e-19,     3.264657837572636e-08,     5.225012389745800e-21,
              2.450797988671408e-05,     1.022606793589491e-40,     5.839375669931880e-06,     1.756177038303328e-01,     2.952913825511192e-01,     2.226322768258314e-17,
              1.301063353984065e-214,    4.619565606283019e-269,    6.440662908392983e-127,    1.086280880700371e-181,    2.299284483690569e-197,    2.896161942590504e-162;
    compare_matrices(covfun.cov(data_4_2(), data_6_2()), result);
}

TEST(SquaredExponential, Derivative)
{
    test_derivative<SquaredExponential>();
}
