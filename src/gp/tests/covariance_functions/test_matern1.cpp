#include <gtest/gtest.h>

#include <gp/covariance_functions/matern1.h>
#include <gp/tests/test_data.hpp>
#include <gp/tests/test_helpers.hpp>

using namespace gp;
using namespace std;


TEST(Matern1, OneDim)
{
    Matern1 covfun(1);
    std::vector<double> params;
    params.push_back(0.5);
    params.push_back(1.5);
    covfun.set_hyper_params(params);

    Eigen::MatrixXd result(4, 4);
    result <<
        2.25,       0.18322,    0.00644488,  0.00134997,
        0.18322,    2.25,       0.0791453,   0.00010993,
        0.00644488, 0.0791453,  2.25,        3.86685e-06,
        0.00134997, 0.00010993, 3.86685e-06, 2.25;
    compare_matrices(covfun.cov(data_4_1()), result); 
}

TEST(Matern1, TwoDim)
{
    Matern1 covfun(2);
    std::vector<double> params;
    params.push_back(0.5);
    params.push_back(0.75);
    params.push_back(1.5);
    covfun.set_hyper_params(params);

    Eigen::MatrixXd result(4, 6);
    result <<
        2.93861e-05, 5.04975e-07, 0.000852682, 3.03049e-05, 5.29858e-05,  0.00170478,
          0.0431201,  0.00044836, 5.07564e-06, 0.000181408,  0.00553236, 0.000131425,
          0.0188782, 2.70792e-06,   0.0141094,    0.235145,     0.29988, 0.000322854,
        5.13922e-14, 1.18969e-15, 7.38229e-11, 6.34905e-13,  1.8686e-13, 3.11898e-12;
    compare_matrices(covfun.cov(data_4_2(), data_6_2()), result);
}

TEST(Matern1, Derivatives)
{
    test_derivative<Matern1>();
}
