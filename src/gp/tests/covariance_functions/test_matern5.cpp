#include <gtest/gtest.h>

#include <gp/covariance_functions/matern5.h>
#include <gp/tests/test_data.hpp>
#include <gp/tests/test_helpers.hpp>

using namespace gp;


TEST(Matern5, OneDim)
{
    Matern5 covfun(1);
    std::vector<double> params;
    params.push_back(0.5);
    params.push_back(1.5);
    covfun.set_hyper_params(params);

    Eigen::MatrixXd result(4, 4);
    result <<
               2.25,    0.141063,  0.00033009, 1.53663e-05,
           0.141063,        2.25,   0.0343095, 9.66431e-08,
         0.00033009,   0.0343095,        2.25,  9.3897e-11,
        1.53663e-05, 9.66431e-08,  9.3897e-11,        2.25;
    compare_matrices(covfun.cov(data_4_1()), result); 
}

TEST(Matern5, TwoDim)
{
    Matern5 covfun(2);
    std::vector<double> params;
    params.push_back(0.5);
    params.push_back(0.75);
    params.push_back(1.5);
    covfun.set_hyper_params(params);

    Eigen::MatrixXd result(4, 6);
    result <<
        6.39377e-09, 1.30031e-12, 6.14139e-06, 6.81413e-09,  2.1588e-08, 2.44239e-05,
          0.0116663, 1.68615e-06, 1.65848e-10, 2.68912e-07, 0.000245691, 1.39273e-07,
          0.0025508, 4.45225e-11,  0.00147536,    0.209819,    0.304884, 8.67812e-07,
        1.21269e-27, 3.33529e-31, 8.32745e-21, 2.84607e-25, 2.00292e-26, 8.95345e-24;
    compare_matrices(covfun.cov(data_4_2(), data_6_2()), result);
}


TEST(Matern5, Derivatives)
{
    test_derivative<Matern5>();
}
