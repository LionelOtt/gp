#include <gtest/gtest.h>

#include <gp/gp/gp_simple.h>
#include <gp/covariance_functions/summing_kernel.h>
#include <gp/covariance_functions/product_kernel.h>
#include <gp/covariance_functions/squared_exponential.h>
#include <gp/covariance_functions/neural_network.h>
#include <gp/covariance_functions/matern3.h>
#include <gp/covariance_functions/matern5.h>
#include <gp/covariance_functions/linear.h>
#include <gp/mean_functions/zero_mean.h>
#include <gp/tests/test_data.hpp>
#include <gp/tests/test_helpers.hpp>

using namespace gp;


TEST(ProductKernel, OneDim)
{
    boost::shared_ptr<SquaredExponential> squared_exp(new SquaredExponential);
    std::vector<double> se_hyper_params;
    se_hyper_params.push_back(2.0);
    se_hyper_params.push_back(3.0);
    squared_exp->set_hyper_params(se_hyper_params);

    boost::shared_ptr<Matern3> matern3(new Matern3);
    std::vector<double> m3_hyper_params;
    m3_hyper_params.push_back(2.0);
    m3_hyper_params.push_back(3.0);
    matern3->set_hyper_params(m3_hyper_params);

    boost::shared_ptr<Matern5> matern5(new Matern5);
    std::vector<double> m5_hyper_params;
    m5_hyper_params.push_back(2.0);
    m5_hyper_params.push_back(3.0);
    matern5->set_hyper_params(m5_hyper_params);

    ProductKernel product_kernel;

    product_kernel.add_cov_func(squared_exp);
    product_kernel.add_cov_func(matern3);
    product_kernel.add_cov_func(matern5);


    Eigen::MatrixXd data = Eigen::MatrixXd::Random(20, 1) * 5.0;
    compare_matrices(
            product_kernel.cov(data),
            (matern3->cov(data).cwiseProduct(
                        matern5->cov(data).cwiseProduct(squared_exp->cov(data))))
    );
}

TEST(ProductKernel, ProductAndSum)
{
    // Test using three-dimensional data.
    unsigned int dim = 3;
    boost::shared_ptr<SquaredExponential> squared_exp(
                                                new SquaredExponential(dim));
    std::vector<double> se_hyper_params;
    se_hyper_params.push_back(2.0);
    se_hyper_params.push_back(3.0);
    se_hyper_params.push_back(4.0);
    se_hyper_params.push_back(5.0);
    squared_exp->set_hyper_params(se_hyper_params);

    boost::shared_ptr<Matern3> matern3(new Matern3(3));
    std::vector<double> m3_hyper_params;
    m3_hyper_params.push_back(4.0);
    m3_hyper_params.push_back(3.0);
    m3_hyper_params.push_back(2.0);
    m3_hyper_params.push_back(4.0);
    matern3->set_hyper_params(m3_hyper_params);

    boost::shared_ptr<Matern5> matern5(new Matern5(3));
    std::vector<double> m5_hyper_params;
    m5_hyper_params.push_back(3.0);
    m5_hyper_params.push_back(2.0);
    m5_hyper_params.push_back(4.0);
    m5_hyper_params.push_back(6.0);
    matern5->set_hyper_params(m5_hyper_params);



    boost::shared_ptr<ProductKernel> prod(new ProductKernel(3));
    prod->add_cov_func(squared_exp);
    prod->add_cov_func(matern3);

    SummingKernel sum(3);

    sum.add_cov_func(prod);
    sum.add_cov_func(matern5);

    Eigen::MatrixXd data = Eigen::MatrixXd::Random(20, 3) * 5.0;
    compare_matrices(
            sum.cov(data),
            (squared_exp->cov(data).cwiseProduct(matern3->cov(data)) +
                                            matern5->cov(data))
    );
}

TEST(ProductKernel, SummingAndProduct)
{
    // Test using three-dimensional data.
    unsigned int dim = 3;
    boost::shared_ptr<SquaredExponential> squared_exp(
                                                new SquaredExponential(dim));
    std::vector<double> se_hyper_params;
    se_hyper_params.push_back(2.0);
    se_hyper_params.push_back(3.0);
    se_hyper_params.push_back(4.0);
    se_hyper_params.push_back(5.0);
    squared_exp->set_hyper_params(se_hyper_params);

    boost::shared_ptr<Matern3> matern3(new Matern3(3));
    std::vector<double> m3_hyper_params;
    m3_hyper_params.push_back(4.0);
    m3_hyper_params.push_back(3.0);
    m3_hyper_params.push_back(2.0);
    m3_hyper_params.push_back(4.0);
    matern3->set_hyper_params(m3_hyper_params);

    boost::shared_ptr<Matern5> matern5(new Matern5(3));
    std::vector<double> m5_hyper_params;
    m5_hyper_params.push_back(3.0);
    m5_hyper_params.push_back(2.0);
    m5_hyper_params.push_back(4.0);
    m5_hyper_params.push_back(6.0);
    matern5->set_hyper_params(m5_hyper_params);

    boost::shared_ptr<SummingKernel> sum(new SummingKernel(3));
    sum->add_cov_func(squared_exp);
    sum->add_cov_func(matern3);

    ProductKernel product_kernel(3);

    product_kernel.add_cov_func(sum);
    product_kernel.add_cov_func(matern5);

    Eigen::MatrixXd data = Eigen::MatrixXd::Random(20, 3) * 5.0;
    compare_matrices(
            product_kernel.cov(data),
            (matern5->cov(data).cwiseProduct(
                        matern3->cov(data) + squared_exp->cov(data)))
    );
}

TEST(ProductKernel, HighDim)
{
    // Test using three-dimensional data.
    unsigned int dim = 3;
    boost::shared_ptr<SquaredExponential> squared_exp(
                                                new SquaredExponential(dim));
    std::vector<double> se_hyper_params;
    se_hyper_params.push_back(2.0);
    se_hyper_params.push_back(3.0);
    se_hyper_params.push_back(4.0);
    se_hyper_params.push_back(5.0);
    squared_exp->set_hyper_params(se_hyper_params);

    boost::shared_ptr<Matern3> matern3(new Matern3(3));
    std::vector<double> m3_hyper_params;
    m3_hyper_params.push_back(4.0);
    m3_hyper_params.push_back(3.0);
    m3_hyper_params.push_back(2.0);
    m3_hyper_params.push_back(4.0);
    matern3->set_hyper_params(m3_hyper_params);

    boost::shared_ptr<Matern5> matern5(new Matern5(3));
    std::vector<double> m5_hyper_params;
    m5_hyper_params.push_back(3.0);
    m5_hyper_params.push_back(2.0);
    m5_hyper_params.push_back(4.0);
    m5_hyper_params.push_back(6.0);
    matern5->set_hyper_params(m5_hyper_params);

    ProductKernel product_kernel(3);

    product_kernel.add_cov_func(squared_exp);
    product_kernel.add_cov_func(matern3);
    product_kernel.add_cov_func(matern5);

    Eigen::MatrixXd data = Eigen::MatrixXd::Random(20, 3) * 5.0;
    compare_matrices(
            product_kernel.cov(data),
            (matern3->cov(data).cwiseProduct(
                        matern5->cov(data).cwiseProduct(squared_exp->cov(data))))
    );
}

TEST(ProductKernel, ClampAndRemap)
{
    // Test using three-dimensional data.
    unsigned int dim = 3;
    boost::shared_ptr<SquaredExponential> squared_exp(
                                                new SquaredExponential(dim));

    squared_exp->fix_param(0,10);

    std::vector<double> se_hyper_params;
    se_hyper_params.push_back(3.0);
    se_hyper_params.push_back(4.0);
    se_hyper_params.push_back(5.0);
    squared_exp->set_free_hyper_params(se_hyper_params);

    boost::shared_ptr<Matern3> matern3(new Matern3(3));

    std::vector<unsigned int> matern3_remapping;
    matern3_remapping.push_back(0);
    matern3_remapping.push_back(0);
    matern3_remapping.push_back(0);
    matern3_remapping.push_back(1);
    matern3->set_remapping(matern3_remapping);

    std::vector<double> m3_hyper_params;
    m3_hyper_params.push_back(4.0);
    m3_hyper_params.push_back(4.0);
    matern3->set_free_hyper_params(m3_hyper_params);

    boost::shared_ptr<Matern5> matern5(new Matern5(3));
    std::vector<double> m5_hyper_params;
    m5_hyper_params.push_back(3.0);
    m5_hyper_params.push_back(2.0);
    m5_hyper_params.push_back(4.0);
    m5_hyper_params.push_back(6.0);
    matern5->set_free_hyper_params(m5_hyper_params);

    ProductKernel product_kernel(3);

    product_kernel.add_cov_func(squared_exp);
    product_kernel.add_cov_func(matern3);
    product_kernel.add_cov_func(matern5);

    Eigen::MatrixXd data = Eigen::MatrixXd::Random(20, 3) * 5.0;
    compare_matrices(
            product_kernel.cov(data),
            (matern3->cov(data).cwiseProduct(
                        matern5->cov(data).cwiseProduct(squared_exp->cov(data))))
    );
}
