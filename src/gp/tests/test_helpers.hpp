#ifndef __GP_TEST_HELPERS_HPP__
#define __GP_TEST_HELPERS_HPP__


#include <fstream>
#include <iostream>


namespace gp
{
    
/**
 * \brief Tests the derivative computations of a covariance function.
 */
template<typename CovFunc>
void test_derivative()
{
    // Test the derivative
    const unsigned int D = 5;
    const unsigned int M = 5000;

    CovFunc covfun(D);
    if(covfun.has_derivatives())
    {
        // Data
        Eigen::MatrixXd X1 = Eigen::MatrixXd::Random(1, D);
        Eigen::MatrixXd X2 = Eigen::MatrixXd::Random(1, D);

        typename CovFunc::ParamVec_t hyper_params;
        for(unsigned int i=0; i<covfun.get_nr_of_params(); ++i)
        {
            hyper_params.push_back(5*(double)rand()/RAND_MAX+1);
        }
        covfun.set_hyper_params(hyper_params);

        Eigen::VectorXd th = Eigen::VectorXd::LinSpaced(M, 0.2, 2);
        Eigen::VectorXd k(M);
        Eigen::VectorXd deriv(M);

        for(unsigned int deriv_i=0; deriv_i<covfun.get_nr_of_params(); ++deriv_i)
        {
            for(unsigned int i=0; i<M; ++i)
            {
                hyper_params[deriv_i] = th(i);
                covfun.set_hyper_params(hyper_params);
                k(i) = covfun.cov(X1, X2)(0, 0);
                deriv(i) = covfun.cov_deriv(X1, X2, deriv_i)(0, 0);
            }
            hyper_params[deriv_i] = 2.0;

            Eigen::VectorXd emp_deriv = (k.tail(M-2) - k.head(M-2)).array()/((th.tail(M-2) - th.head(M-2)).array());

            if (((emp_deriv - deriv.segment(1,M-2)).array()/emp_deriv.array()).cwiseAbs().maxCoeff() > 0.001)
            {
                std::cout << "Failed at deriv_i = " << deriv_i << std::endl;
                std::cout << "Absolute error = " << (emp_deriv - deriv.segment(1,M-2)).cwiseAbs().maxCoeff() << std::endl;
                std::cout << "Relative error = " << ((emp_deriv - deriv.segment(1,M-2)).array()/emp_deriv.array()).cwiseAbs().maxCoeff() << std::endl;

                std::ofstream data("/tmp/data.dat");
                data << k.segment(1,M-2).transpose() << std::endl;
                data << th.segment(1,M-2).transpose() << std::endl;
                data << emp_deriv.transpose() << std::endl;
                data << deriv.segment(1,M-2).transpose() << std::endl;

                FAIL();
            }
        }
    }
    else
    {
        std::cout << "This covariance function has not implemented derivatives." <<std::endl;
    }
}

void compare_matrices(Eigen::MatrixXd const& a, Eigen::MatrixXd const& b, double eps=0.0001)
{
    assert(a.rows() == b.rows());
    assert(a.cols() == b.cols());

    for(int i=0; i<a.rows(); ++i)
    {
        for(int j=0; j<a.cols(); ++j)
        {
            EXPECT_NEAR(a(i, j), b(i, j), eps);
       }
    }
}

} /* gp */

#endif /* __GP_TEST_HELPERS_HPP__ */
