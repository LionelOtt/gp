#include <gtest/gtest.h>

#include <boost/make_shared.hpp>

#include <gp/gp/gp_large_data.h>
#include <gp/covariance_functions/space_time_kernel.h>
#include <gp/covariance_functions/summing_kernel.h>
#include <gp/covariance_functions/matern3.h>
#include <gp/covariance_functions/matern5.h>
#include <gp/covariance_functions/periodic_exponential.h>
#include <gp/noise_functions/stationary_noise.h>
#include <gp/mean_functions/stationary_mean.h>
#include <gp/tests/test_data.hpp>
#include <gp/tests/test_helpers.hpp>

using namespace gp;

TEST(GPLargeData, OneDim)
{
    unsigned int spatial_dimensions = 2;

    //Build covariance functions
    boost::shared_ptr<Matern3> matern3(new Matern3(spatial_dimensions));

    std::vector<unsigned int> mat3_remapped_params;
    mat3_remapped_params.push_back(0);
    mat3_remapped_params.push_back(0);
    mat3_remapped_params.push_back(1);
    matern3->set_remapping(mat3_remapped_params);

    std::vector<double> m3_hyper_params;
    m3_hyper_params.push_back(1.8620);
    m3_hyper_params.push_back(1.1950);
    matern3->set_free_hyper_params(m3_hyper_params);

    boost::shared_ptr<Matern5> matern5(new Matern5());
    std::vector<double> m5_hyper_params;
    m5_hyper_params.push_back(-0.201);
    m5_hyper_params.push_back(5.8460);
    matern5->set_hyper_params(m5_hyper_params);

    boost::shared_ptr<PeriodicExponential> per_exp(new PeriodicExponential());

    per_exp->fix_param(1,1);

    std::vector<double> per_exp_hyper_params;
    per_exp_hyper_params.push_back(0.9390);
    per_exp_hyper_params.push_back(5.7530);
    per_exp->set_free_hyper_params(per_exp_hyper_params);

    SummingKernel summing_kernel;
    summing_kernel.add_cov_func(matern5);
    summing_kernel.add_cov_func(per_exp);

    SpaceTimeKernel space_time_kernel;
    space_time_kernel.set_time_cov_func(
                            boost::make_shared<SummingKernel>(summing_kernel));
    space_time_kernel.set_space_cov_func(matern3);

    StationaryMean mean_func(3);
    mean_func.fix_param(0,45.5971);

    StationaryNoise noise_func(3);
    noise_func.fix_param(0,2.12);

    // Create a new GP
    GPLargeData gp(
        boost::shared_ptr<SpaceTimeKernel>(
                        boost::make_shared<SpaceTimeKernel>(space_time_kernel)),
        boost::shared_ptr<StationaryMean>(
                        boost::make_shared<StationaryMean>(mean_func)),
        boost::shared_ptr<StationaryNoise>(
                        boost::make_shared<StationaryNoise>(noise_func))
    );

    Eigen::MatrixXd X = data_x_space_time();
    Eigen::MatrixXd X_test = data_x_test_space_time();
    Eigen::MatrixXd y = data_y_space_time();
    Eigen::MatrixXd y_matlab = data_y_test_matlab_space_time();

    // Set the training data
    gp.set_training_data(X,y);

    // Set the testing points
    gp.set_query_points(X_test);

    // Get the predictions from the GP
    GPResult output = gp.inference();

    compare_matrices(output.mean, y_matlab.col(0), 0.1);
    compare_matrices(output.variance, y_matlab.col(1), 0.1);

}
