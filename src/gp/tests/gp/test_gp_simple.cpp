#include <gtest/gtest.h>

#include <gp/gp/gp_simple.h>
#include <gp/covariance_functions/squared_exponential.h>
#include <gp/mean_functions/zero_mean.h>
#include <gp/noise_functions/stationary_noise.h>

using namespace gp;

TEST(GPSimple, OneDim)
{
    // Create a new GP
    GPSimple gp(
        boost::shared_ptr<SquaredExponential>(new SquaredExponential()),
        boost::shared_ptr<ZeroMean>(new ZeroMean()),
        boost::shared_ptr<StationaryNoise>(new StationaryNoise())
    );

}
