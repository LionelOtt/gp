#include <iostream>
#include <fstream>

#include <gp/covariance_functions/squared_exponential.h>
#include <gp/gp/gp_simple.h>
#include <gp/mean_functions/zero_mean.h>
#include <gp/noise_functions/stationary_noise.h>
#include <gp/gp/optimisers/nlopt_optimiser.h>
#include <gp/tests/test_data.hpp>

using namespace gp;
using namespace std;

int main(int argc, char *argv[])
{
    // Create a new GP
    GPSimple gp(
        boost::shared_ptr<SquaredExponential>(new SquaredExponential()),
        boost::shared_ptr<ZeroMean>(new ZeroMean()),
        boost::shared_ptr<StationaryNoise>(new StationaryNoise())
    );

    std::vector<unsigned int> remapped_params;
    remapped_params.push_back(0);
    remapped_params.push_back(0);
    gp.get_cov_func()->set_remapping(remapped_params);

    gp.get_cov_func()->fix_param(1,1.4);

    // Set initial parameters
    std::vector<double> free_hyper_params;
    free_hyper_params.push_back(1.5);
    //gp.set_free_hyper_params(free_hyper_params);

    //push the noise
    free_hyper_params.push_back(0.4);


    // Create training data and labels (from the Rasmusen example)
    Eigen::MatrixXd X(20, 1);
    Eigen::ArrayXd y(20);
    X << -2.1775, -0.9235,  0.7502, -5.8868, -2.7995,
          4.2504,  2.4582,  6.1426, -4.0911, -6.3481,
          1.0004, -4.7591,  0.4715,  4.8933,  4.3248,
         -3.7461, -7.3005,  5.8177,  2.3851, -6.3772;
    y <<  1.4121,  1.6936, -0.7444,  0.2493,  0.3978,
         -1.2755, -2.2210, -0.8452, -1.2232,  0.0105,
         -1.0258, -0.8207, -0.1462, -1.5637, -1.0980,
         -1.1721, -1.7554, -1.0712, -2.6937, -0.0329;

    // Feed the data to the GP
    gp.set_training_data(X,y);

    // Optimise the hyper parameters
    NLoptOptimiser optimiser;
    optimiser.set_verbose(true);
    optimiser.optimise(gp);

    return 0;
}
