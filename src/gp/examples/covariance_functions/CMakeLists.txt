add_executable( example_fix_and_remap_params example_fix_and_remap_params.cpp )
target_link_libraries(
    example_fix_and_remap_params
    ${LIB_GP}
    ${LIB_GOALFUNC}
    ${LIB_COVFUNC}
    ${LIB_MEANFUNC}
    ${LIB_OPTIMISER}
    ${NLOPT_LIBRARIES}
)
