add_subdirectory(covariance_functions)

add_executable (
    example_nlopt
    nlopt.cpp
)

target_link_libraries (
    example_nlopt
    ${LIB_COVFUNC}
    ${LIB_MEANFUNC}
    ${NLOPT_LIBRARIES}
)

add_executable( example_gp_sr_learn gp_sr_learn.cpp )
target_link_libraries(
    example_gp_sr_learn
    ${LIB_GP}
    ${LIB_GOALFUNC}
    ${LIB_COVFUNC}
    ${LIB_MEANFUNC}
    ${LIB_NOISEFUNC}
    ${LIB_OPTIMISER}
    ${NLOPT_LIBRARIES}
)

add_executable( example_gp_learn gp_learn.cpp )
target_link_libraries(
    example_gp_learn
    ${LIB_GP}
    ${LIB_GOALFUNC}
    ${LIB_COVFUNC}
    ${LIB_MEANFUNC}
    ${LIB_NOISEFUNC}
    ${LIB_OPTIMISER}
    ${NLOPT_LIBRARIES}
)


add_executable( example_inference inference.cpp )
target_link_libraries(
    example_inference
    ${LIB_GP}
    ${LIB_COVFUNC}
    ${LIB_MEANFUNC}
    ${LIB_NOISEFUNC}
)


add_executable( example_simulated_annealing simulated_annealing.cpp )
target_link_libraries(
    example_simulated_annealing
    ${LIB_GP}
    ${LIB_GOALFUNC}
    ${LIB_COVFUNC}
    ${LIB_MEANFUNC}
    ${LIB_NOISEFUNC}
    ${LIB_OPTIMISER}
)
