#include <iostream>
#include <fstream>

#include <gp/gp/gp.h>

using namespace gp;
using namespace std;

int main(int argc, char *argv[])
{
    // Create a new GP
    GPSimple gp(
        boost::shared_ptr<SquaredExponential>(new SquaredExponential()),
        boost::shared_ptr<StationaryMean>(new StationaryMean()),
        boost::shared_ptr<StationaryNoise>(new StationaryNoise())
    );

    //Mean hyper-params
    std::vector<double> mean_hyper_params;
    mean_hyper_params.push_back(0);
    gp.set_mean_hyper_params(mean_hyper_params);

    //Cov hyper-params
    std::vector<double> cov_hyper_params;
    cov_hyper_params.push_back(0.5);
    cov_hyper_params.push_back(0.5);
    gp.set_cov_hyper_params(cov_hyper_params);

    //Noise hyper-params
    std::vector<double> noise_hyper_params;
    noise_hyper_params.push_back(0.1);
    gp.set_noise_hyper_params(noise_hyper_params);

    // Create training data and labels (from the Rasmusen example)
    Eigen::MatrixXd X(20, 1);
    Eigen::ArrayXd y(20);
    X << -2.1775, -0.9235,  0.7502, -5.8868, -2.7995,
          4.2504,  2.4582,  6.1426, -4.0911, -6.3481,
          1.0004, -4.7591,  0.4715,  4.8933,  4.3248,
         -3.7461, -7.3005,  5.8177,  2.3851, -6.3772;
    y <<  1.4121,  1.6936, -0.7444,  0.2493,  0.3978,
         -1.2755, -2.2210, -0.8452, -1.2232,  0.0105,
         -1.0258, -0.8207, -0.1462, -1.5637, -1.0980,
         -1.1721, -1.7554, -1.0712, -2.6937, -0.0329;

    // Feed the data to the GP
    gp.set_training_data(X,y);

    // Optimise the hyper parameters
    NLoptOptimiser optimiser;
    optimiser.set_verbose(true);
    optimiser.optimise(gp);

    // Create a set of points to perform predictions for
    Eigen::MatrixXd test_points(201,1);
    test_points.col(0) = Eigen::ArrayXd::LinSpaced(201, -7.5f, 7.5f);

    // Get the predictions from the GP
    gp.set_query_points(test_points);
    GPResult output = gp.inference();

    // Write data and predictions to the harddisk for plotting
    ofstream data("/tmp/data.dat");
    data << X.transpose() << endl;
    data << y.transpose() << endl;
    ofstream pred("/tmp/pred.dat");
    pred << test_points.col(0).transpose() << endl;
    pred << output.mean.transpose() << endl;
    pred << output.variance.transpose() << endl;

    return 0;
}
