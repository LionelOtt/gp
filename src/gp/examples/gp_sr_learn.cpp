#include <iostream>
#include <fstream>

#include <gp/gp/gp.h>


using namespace gp;
using namespace std;

int main(int argc, char *argv[])
{
    // Create a new GP
    GPSR gp(
        boost::shared_ptr<SquaredExponential>(new SquaredExponential()),
        boost::shared_ptr<StationaryMean>(new StationaryMean()),
        boost::shared_ptr<StationaryNoise>(new StationaryNoise())
    );

    // Initial hyperparameters
    std::vector<double> hyper_params;
    hyper_params.push_back(-0.5958);

    // Covariance hyperparameters
    hyper_params.push_back(0.5);
    hyper_params.push_back(0.5);

    // Lastly push the noise
    hyper_params.push_back(0.1);

    gp.set_gp_hyper_params(hyper_params);

    // Create training data and labels (from the Rasmusen example)
    Eigen::MatrixXd X(20, 1);
    Eigen::ArrayXd y(20);
    X << -2.1775, -0.9235,  0.7502, -5.8868, -2.7995,
          4.2504,  2.4582,  6.1426, -4.0911, -6.3481,
          1.0004, -4.7591,  0.4715,  4.8933,  4.3248,
         -3.7461, -7.3005,  5.8177,  2.3851, -6.3772;
    y <<  1.4121,  1.6936, -0.7444,  0.2493,  0.3978,
         -1.2755, -2.2210, -0.8452, -1.2232,  0.0105,
         -1.0258, -0.8207, -0.1462, -1.5637, -1.0980,
         -1.1721, -1.7554, -1.0712, -2.6937, -0.0329;

    // Feed the data to the GP
    gp.set_training_data(X,y);

    Eigen::MatrixXd inducing_points = Eigen::ArrayXd::LinSpaced(10, -7.5f, 7.5f);
    // Set inducing points
    gp.set_inducing_points(inducing_points);

    // Optimise the hyper parameters
    NLoptOptimiser optimiser;
    optimiser.set_verbose(true);
    optimiser.optimise(gp);

    // Create a set of points to perform predictions for
    Eigen::MatrixXd test_points(201,1);
    test_points.col(0) = Eigen::ArrayXd::LinSpaced(201, -7.5f, 7.5f);

    gp.set_query_points(test_points);

    // Get the predictions from the GP
    GPResult output = gp.inference();

    return 0;
}
