#include <fstream>
#include <iostream>

#include <gp/util/matrix_importer.h>



namespace gp
{


Eigen::MatrixXd import_matrix(std::string const& fpath)
{
    std::ifstream filestream(fpath.c_str());

    if(filestream.good())
    {
        // Determine the number of columns
        std::string str;
        std::getline(filestream, str);
        std::stringstream ss(str);
        double tmp;
        int cols = 0;
        while(ss.good())
        {
            ss >> tmp;
            ++cols;
        }
        filestream.seekg(0, std::ios::beg);

        // Determine the number of rows
        int rows = 0;
        while(filestream.peek() != EOF)
        {
            std::getline(filestream,str);
            ++rows;
        }
        filestream.clear();
        filestream.seekg(0, std::ios::beg);

        Eigen::MatrixXd matrix(rows, cols);
        for(int i=0; i<rows; ++i)
        {
            for(int j=0; j<cols; ++j)
            {
                filestream >> matrix(i,j);
            }
        }
        return matrix;
    }
    else
    {
        return Eigen::MatrixXd(0,0);
    }
}


} /* gp */
