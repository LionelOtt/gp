#ifndef __GP_PARAMETRIC_FUNCTION_H__
#define __GP_PARAMETRIC_FUNCTION_H__

#include <vector>
#include <Eigen/Core>

namespace gp
{

/**
 * \brief Generic parametric function.
 *
 * This class represents a generic parametric function.
 * It handles a parameter vector and is able to fix and remap parameters.
 */
class ParametricFunction
{
    public:
        //! Typedef for the storage of parameters
        typedef std::vector<double> ParamVec_t;

    public:
        ParametricFunction();
        virtual ~ParametricFunction();

    public:

        /**
         * \brief Returns the number of hyperparameters of the function
         *
         * \return The number of hyperparameters
         **/
        virtual unsigned int get_nr_of_params() = 0;

        /**
          * \brief Returns the name of the function
          **/
        virtual std::string get_name() = 0;

        /**
         * \brief Checks if the dimensionality of the parameters is coherent
         *        to what is expected by the function.
         *
         * In case that the dimensionality is not expected, an error will
         * be thrown.
         * Virtual function to allow redefinition when necessary.
         *
         * \param params Array of parameters for the function.
         */
        virtual void dimensionality_check_params(ParamVec_t const& params);

        /**
         * \brief Sets the parameters of the function.
         *
         * The length of the input must be "complete", i.e. of the expected
         * size depending on each function. The number only depends on the
         * function itself and does not depend on any remapped or
         * fixed parameters.
         * Virtual function to allow redefinition when necessary (for example
         * in the summing kernel class).
         *
         * \param params The parameters to be set.
         * \param force  Forces the hyperparameter setting, removing fixed and
         *               remapped parameters.
         */
        virtual void set_hyper_params(
                                      ParamVec_t const& params,
                                      bool force = false
        );

        /**
         * \brief Sets the free parameters of the function.
         *
         * The length of the input must be only for free parameters, i.e
         * depends on the number of remapped and/or fixed parameters. Ideal for
         * with optimisers, because only free params can be modified.
         * Virtual function to allow redefinition when necessary (for example
         * in the summing kernel class).
         *
         * \param params The parameters to be set.
         */
        virtual void set_free_hyper_params(ParamVec_t const& params);

        /**
         * \brief Returns the parameters of the function.
         *
         * The length of the output is "complete", i.e. of the expected
         * size depending on each covariance function. The output will include
         * fixed and remapped hyperparameters.
         * Virtual function to allow redefinition when necessary (for example
         * in the summing kernel class).
         *
         * \return Parameters of the function
         */
        virtual ParamVec_t get_hyper_params() const;

        /**
         * \brief Returns the free parameters of the function.
         *
         * The length of the out constains only free parameters, i.e
         * depends on the number of remapped and/or fixed parameters.
         * Ideal for use with optimiser, as only returns free parameters.
         * Virtual function to allow redefinition when necessary (for example
         * in the summing kernel class).
         *
         * \return Free parameters of the function
         */
        virtual ParamVec_t get_free_hyper_params();

        /**
         * \brief Returns the number of free parameters of the function.
         *
         * The output number depends only on free parameters, i.e
         * depends indirectly on the number of remapped and/or fixed parameters.
         * Ideal for use with optimiser, as only returns free parameters.
         * Virtual function to allow redefinition when necessary (for example
         * in the summing kernel class).
         *
         * \return Free parameters of the function
         */
        virtual unsigned int get_nr_of_free_params();

        /**
         * \brief Fixes a param to a particular value. Not modifiable by the
         * optimiser.
         *
         * When a remapping exists, all of the remapped indexes to the input
         * index will get their values modified and fixed as a consequence.
         * Virtual function to allow redefinition when necessary (for example
         * in the summing kernel class).
         *
         * \param index Index of the parameter to be fixed (index in the range
         *              of total number of parameters), without considering
         *              previously remapped or clamped values.
         * \param value New value of the parameter.
         */
        virtual void fix_param(unsigned int index, double value);

        /**
         * \brief Sets a remapping for the array of indexes.
         *
         * Essentially it receives a vector of length equal to the total number
         * of parameters for the covariance function. The indexes should start
         * at cero and be integers.
         * Virtual function to allow redefinition when necessary (for example
         * in the summing kernel class).
         *
         * Example: Imagine a covariance function that has 4 parameters:
         *          [a b c d]
         *          A remapping of this parameters can be the vector:
         *          [0 0 1 1]
         *          After setting this remapping, the first and second hyper-
         *          parameters will be coupled togheter and act as one free
         *          parameter. Same will happen with the third and fourth
         *          hyperparameters.
         *          Not acceptable remapping vectors are:
         *          [2 0 0 0] or [0 0 0 2] (there should be consecutive values
         *          inside the remapping vector).
         *
         * \param remapping_vector Vector that remaps the indexes of the
         *                         hyperparameters of a covariance function.
         */
        virtual void set_remapping(std::vector<unsigned int> remapping_vector);

        /**
         * @brief Reset parameters of a covariance function.
         * Virtual function to allow redefinition when necessary (for example
         * in the summing kernel class).
         */
        virtual void reset_params();

        virtual ParamVec_t get_hyper_params_from_free_params(ParamVec_t free_params);

    protected:

        std::vector<unsigned int> get_unique_remapped_idx(
                std::vector<unsigned int> remapping_vector);

        void update_free_params();

    protected:

        /** All of the parameters of the covariance function
         *  including fixed and remapped ones.
         */
        ParamVec_t                      m_params;

        std::vector<bool>               m_idx_free;

        //! Remapping of hyperparameters (can be used to build isotropic
        //! covariance functions.
        std::vector<unsigned int> m_idx_remapped;

        std::vector<unsigned int> m_idx_fixed;
};

} /* gp */

#endif /* __GP_ABSTRACT_COVARIANCE_FUNCTION_H__ */
