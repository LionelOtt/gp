#ifndef __GP_MATRIX_IMPORTER_H__
#define __GP_MATRIX_IMPORTER_H__


#include <string>

#include <Eigen/Core>


namespace gp
{


/**
 * \brief Reads matrix content from a textfile.
 *
 * The data is assumed to contain one matrix row per line and each entry to be
 * separated by a white space.
 *
 * \param fpath path to the textfile from which to read the data
 * \return matrix containing the data stored in the given file
 */
Eigen::MatrixXd import_matrix(std::string const& fpath);


} // gp

#endif // __GP_MATRIX_IMPORTER_H__
