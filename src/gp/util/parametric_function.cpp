#include <iostream>

#include <gp/util/parametric_function.h>
#include <gp/gp/error.hpp>

namespace gp
{

ParametricFunction::ParametricFunction()
{

}

ParametricFunction::~ParametricFunction()
{

}

void ParametricFunction::dimensionality_check_params(ParamVec_t const& params)
{
    if(params.size() != get_nr_of_params())
    {
        std::stringstream error;
        error << "Wrong number of parameters provided: "
              << "Expected "
              << get_nr_of_params()
              << " but "
              << params.size()
              << " where given. ";
        throw ParameterException(error.str());
    }
}

void ParametricFunction::set_hyper_params(ParamVec_t const& params,bool force)
{
    dimensionality_check_params(params);

    if(force)
    {
        reset_params();
        m_params = params;
    }
    else
    {
        //Check if they agree with the remapped parameters.
        std::vector<unsigned int> unique_idx =
                get_unique_remapped_idx(m_idx_remapped);


        //No remapping has been set if unique_idx has the same dimension
        //as the number of params.
        if(unique_idx.size() < get_nr_of_params())
        {
            // For each unique_idx find all of their appearence and check if
            // they are all the same.
            for(unsigned int i = 0; i < unique_idx.size(); i++)
            {
                unsigned int current_unique_id = unique_idx[i];
                // Find the appearences of this id in the param vector
                std::vector<unsigned int> appearences;
                for(unsigned int j = 0; j < m_idx_remapped.size(); j++)
                {
                    if(m_idx_remapped[j] == current_unique_id)
                    {
                        appearences.push_back(j);
                    }
                }
                if(appearences.size() < 1)
                {
                    throw ParameterException("ParametricFunction: "
                                         "Error inside remapped index array.");
                }
                double value = params[appearences[0]];
                for(unsigned int j = 0; j < appearences.size(); j++)
                {
                    if(params[appearences[j]] != value)
                    {

                        throw ParameterException("ParametricFunction :"
                                             "params not compatible with "
                                             "current remapping");
                    }
                }
            }
        }
        else
        {
            m_params = params;
        }
    }
}

void ParametricFunction::set_free_hyper_params(ParamVec_t const& params)
{
    if(params.size() != get_nr_of_free_params())
    {
        throw ParameterException("ParametricFunction: "
                             "Number of free params does not match.");
    }

    unsigned int idx = 0;
    for(unsigned int i = 0; i < get_nr_of_params(); i++)
    {
        //Find all ocurrances of i inside the remapping index
        std::vector<unsigned int> appearences;
        for(unsigned int j = 0; j < m_idx_remapped.size(); j++)
        {
            if(m_idx_remapped[j] == i)
            {
                appearences.push_back(j);
            }
        }
        bool at_least_one_free = false;
        //Check if any of those appearences is free
        for(unsigned int j = 0; j < appearences.size(); j++)
        {
            if(m_idx_free[appearences[j]])
            {
                at_least_one_free = true;
                break;
            }
        }
        if(at_least_one_free)
        {
            //Replace all repetitions of that param depending on the
            //remapping
            for(unsigned int j = 0; j < appearences.size(); j++)
            {
                m_params[appearences[j]] = params[idx];
            }
            idx++;
        }
    }
}

ParametricFunction::ParamVec_t  ParametricFunction::get_hyper_params() const
{
    return m_params;
}

ParametricFunction::ParamVec_t  ParametricFunction::get_free_hyper_params()
{
    ParamVec_t free_params;
    free_params.clear();
    unsigned int n_pars = get_nr_of_params();

    for(unsigned int i = 0; i < n_pars; i++)
    {
        if(m_idx_free[i])
        {
            free_params.push_back(m_params[i]);
        }
    }

    return free_params;
}

unsigned int ParametricFunction::get_nr_of_free_params()
{

    return get_free_hyper_params().size();
}

void ParametricFunction::fix_param(unsigned int index, double value)
{
    if(index > get_nr_of_params())
    {
        throw ParameterException("ParametricFunction: "
                             "Index larger than number of params.");
    }

    m_idx_fixed.push_back(index);
    m_params[index] = value;

    //Go through the remapping array and fix all associated values
    unsigned int remapped_index = m_idx_remapped[index];
    for(unsigned int i = 0; i < get_nr_of_params(); i++)
    {
        if(m_idx_remapped[i] == remapped_index)
        {
            m_params[i] = value;
        }
    }

    update_free_params();
}

void ParametricFunction::set_remapping(
        std::vector<unsigned int> remapping_vector
)
{
    if(remapping_vector.size() != get_nr_of_params())
    {
        throw ParameterException("ParametricFunction: "
                             "Remapping vector of the wrong size.");
    }

    std::vector<unsigned int> unique =
            get_unique_remapped_idx(remapping_vector);

    unsigned int min_index = 0;
    unsigned int max_index = 0;
    for(unsigned int i = 0; i < get_nr_of_params(); i++)
    {
        unsigned int remapped_index = remapping_vector[i];
        if(i == 0)
        {
            min_index = remapped_index;
            max_index = remapped_index;
        }
        else
        {
            if(remapped_index < min_index)
            {
                min_index = remapped_index;
            }
            if(remapped_index > max_index)
            {
                max_index = remapped_index;
            }
        }
    }
    if(min_index != 0)
    {
        throw ParameterException("ParametricFunction: "
                             "Min of remapping vector has to be zero");
    }
    if(max_index != unique.size()-1)
    {
        throw ParameterException("ParametricFunction: "
                             "Remapping must contain consecutive values");
    }

    m_idx_remapped = remapping_vector;

    //Check for all fixed params and copy their values
    for(unsigned int i = 0; i < m_idx_fixed.size(); i++)
    {
        double value = m_params[m_idx_fixed[i]];
        unsigned int remapped_idx = m_idx_remapped[m_idx_fixed[i]];
        for(unsigned int j = 0; j < get_nr_of_params(); j++)
        {
            if(m_idx_remapped[j] == remapped_idx)
            {
                m_params[j] = value;
            }
        }
    }

    update_free_params();
}

void ParametricFunction::reset_params()
{
    m_params.clear();
    //Initialisation of param vector, all in ones.
    for(unsigned int i = 0; i < get_nr_of_params(); i++)
    {
        m_params.push_back(1);
    }

    m_idx_remapped.clear();
    //Initialisation of remapping vector, in consecutive order.
    for(unsigned int i = 0; i < get_nr_of_params(); i++)
    {
        m_idx_remapped.push_back(i);
    }

    m_idx_free.clear();
    //Initialisation of remapping vector, in consecutive order.
    for(unsigned int i = 0; i < get_nr_of_params(); i++)
    {
        m_idx_free.push_back(true);
    }

    m_idx_fixed.clear();
}

ParametricFunction::ParamVec_t
   ParametricFunction::get_hyper_params_from_free_params(ParamVec_t free_params)
{
    ParamVec_t full_params = m_params;

    std::vector<unsigned int> unique_idx =
                get_unique_remapped_idx(m_idx_remapped);

    //counter for adding free parameters
    unsigned int free_params_added = 0;
    for(unsigned int i = 0; i < unique_idx.size(); i++)
    {
        unsigned int current_unique_id = unique_idx[i];
        bool add_from_free_param = true;
        // Find the appearences of this id in the param vector
        std::vector<unsigned int> appearences;
        for(unsigned int j = 0; j < m_idx_remapped.size(); j++)
        {
            if(m_idx_remapped[j] == current_unique_id)
            {
                appearences.push_back(j);
            }
        }
        // Check if none of the appearences has been fixed
        for(unsigned int j = 0; j < appearences.size(); j++)
        {
            for(unsigned int k = 0; k < m_idx_fixed.size(); k++)
            {
                if(m_idx_fixed[k] == appearences[j])
                {
                    add_from_free_param = false;
                }
            }
        }
        // This group of parameters hasn't been fixed.
        if(add_from_free_param)
        {
            for(unsigned int j = 0; j < appearences.size(); j++)
            {
                full_params[appearences[j]] = free_params[free_params_added];
                if(j == (appearences.size()-1))
                {
                    free_params_added++;
                }
            }
        }
    }

    return full_params;
}

std::vector<unsigned int> ParametricFunction::get_unique_remapped_idx(
        std::vector<unsigned int> remapping_vector)
{
    // Find the number of different indexes in the remaped array
    std::vector<unsigned int> unique_idx;
    for(unsigned int i = 0; i < remapping_vector.size(); i++)
    {
        bool is_repeated = false;
        for(unsigned int j = 0; j < unique_idx.size(); j++)
        {
            if(unique_idx[j] == remapping_vector[i])
            {
                is_repeated = true;
                break;
            }
        }
        if(!is_repeated)
        {
            unique_idx.push_back(remapping_vector[i]);
        }
    }
    return unique_idx;
}


void ParametricFunction::update_free_params()
{
    //Assume initially all of the params are free and then only fix the ones
    //that are fixed directly or indirectly.
    m_idx_free.assign(get_nr_of_params(),true);

    std::vector<unsigned int> unique_idx =
            get_unique_remapped_idx(m_idx_remapped);

    // For each unique_idx find all of their appearence and check if any of
    // them is not free
    for(unsigned int i = 0; i < unique_idx.size(); i++)
    {
        unsigned int current_unique_id = unique_idx[i];
        bool fixed_params = false;
        // Find the appearences of this id in the param vector
        std::vector<unsigned int> appearences;
        for(unsigned int j = 0; j < m_idx_remapped.size(); j++)
        {
            if(m_idx_remapped[j] == current_unique_id)
            {
                appearences.push_back(j);
            }
        }
        if(appearences.size() < 1)
        {
            throw ParameterException("AbstractCovarianceFunction: "
                                 "Error inside remapped index array.");
        }
        //Check if any of the appearences are fixed
        for(unsigned int j = 0; j < appearences.size(); j++)
        {
            for(unsigned int k = 0; k < m_idx_fixed.size(); k++)
            {
                if(m_idx_fixed[k] == appearences[j])
                {
                    fixed_params = true;
                }
            }
        }
        //All the appearences are set to not free
        for(unsigned int j = 0; j < appearences.size(); j++)
        {
            m_idx_free[appearences[j]] = false;
        }
        if(!fixed_params)
        {
            //If the current id is not fixed then only the first one is set to
            //free
            m_idx_free[appearences[0]] = true;
        }
    }
}

} /* gp */
