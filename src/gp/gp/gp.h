
//! Including GP model files
#include <gp/gp/gp_large_data.h>
#include <gp/gp/gp_result.h>
#include <gp/gp/gp_simple.h>
#include <gp/gp/gp_sr.h>

//! Including GP covariance functions
#include <gp/covariance_functions/exponential.h>
#include <gp/covariance_functions/linear.h>
#include <gp/covariance_functions/matern1.h>
#include <gp/covariance_functions/matern3.h>
#include <gp/covariance_functions/matern5.h>
#include <gp/covariance_functions/neural_network.h>
#include <gp/covariance_functions/periodic_exponential.h>
#include <gp/covariance_functions/polynomial.hpp>
#include <gp/covariance_functions/product_kernel.h>
#include <gp/covariance_functions/rational_quadratic.h>
#include <gp/covariance_functions/space_time_kernel.h>
#include <gp/covariance_functions/squared_exponential.h>
#include <gp/covariance_functions/summing_kernel.h>

//! Including GP mean function
#include <gp/mean_functions/stationary_mean.h>
#include <gp/mean_functions/zero_mean.h>

//! Including GP noise function
#include <gp/noise_functions/stationary_noise.h>

//! Include GP optimisers
#include <gp/gp/optimisers/nlopt_optimiser.h>
#include <gp/gp/optimisers/simulated_annealing.hpp>
