#include <gp/gp/gp_sr.h>
/* For debug
#include <iostream>
#include <fstream>
*/

namespace gp
{

class {
public:
    template<typename T>
    operator boost::shared_ptr<T>() { return boost::shared_ptr<T>(); }
} nullPtr;

GPSR::GPSR()
    :AbstractGaussianProcess(nullPtr,nullPtr,nullPtr)
{

}

GPSR::GPSR(
        boost::shared_ptr<AbstractCovarianceFunction> cov_func,
        boost::shared_ptr<AbstractMeanFunction> mean_func,
        boost::shared_ptr<AbstractNoiseFunction> noise_func
)
    :AbstractGaussianProcess(cov_func,mean_func,noise_func)
{

}

void GPSR::set_gp_hyper_params(
        ParametricFunction::ParamVec_t const& params
)
{
    //Only do something if parameters have changed.
    if(get_gp_hyper_params() != params)
    {
        AbstractGaussianProcess::set_gp_hyper_params(params);
        //Recalculates covariances to achieve faster prediction.
        recalculate_covariances();
    }
}


void GPSR::set_free_gp_hyper_params(
        ParametricFunction::ParamVec_t const& params
)
{
    if(get_free_gp_hyper_params() != params)
    {
        AbstractGaussianProcess::set_free_gp_hyper_params(params);
        //Recalculates covariances to achieve faster prediction.
        recalculate_covariances();
    }

}

void GPSR::set_training_data(
        Eigen::MatrixXd const&          data,
        Eigen::VectorXd const&          labels
)
{
    if(data.rows() != labels.rows())
    {
        throw DimensionException("Row count of data and labels does not match");
    }
    m_training = data;
    m_labels = labels;

    //Check if cov_func exists
    if(get_cov_func())
    {
        recalculate_covariances();
    }
}

void GPSR::clear_training_data()
{
    m_training = Eigen::MatrixXd();
    m_labels = Eigen::VectorXd();
    m_cov_ind_ind = Eigen::MatrixXd();
    m_cov_train_ind = Eigen::MatrixXd();
    m_cov_query_ind = Eigen::MatrixXd();
}


void GPSR::append_training_data(
        Eigen::MatrixXd const&          data,
        Eigen::VectorXd const&          labels
)
{
    if(data.rows() != labels.rows())
    {
        throw DimensionException("Row count of new data does not match");
    }
    if(m_training.rows() > 0 && data.cols() != m_training.cols())
    {
        throw DimensionException("Dimensionality of old and new data does not match");
    }

    int new_rowcount = m_training.rows() + data.rows();

    // Resize data storage
    if(m_training.cols() == 0)
    {
        m_training.conservativeResize(data.rows(), data.cols());
    }
    else
    {
        m_training.conservativeResize(new_rowcount, m_training.cols());
    }
    m_labels.conservativeResize(new_rowcount);

    // Append new data at the end
    m_training.bottomRows(data.rows()) = data;
    m_labels.tail(labels.rows()) = labels;

    recalculate_covariances();
}

void GPSR::remove_last_training_point()
{
    // Check if there is something to remove
    if(m_training.rows() == 0 || m_labels.rows() == 0)
    {
        throw DimensionException("There is no old data to remove");
    }

    // Remove data and label
    m_training.conservativeResize(m_training.rows()-1, m_training.cols());
    m_labels.conservativeResize(m_labels.rows()-1);

    // Downdate covariance between training data and induced points
    m_cov_train_ind.conservativeResize(
            m_cov_train_ind.rows()-1, m_inducing_points.rows()
                );
    m_cov_query_ind.conservativeResize(
            m_cov_query_ind.rows()-1, m_query.rows()
    );
}

void GPSR::set_training_labels(Eigen::VectorXd const& labels)
{
    if(labels.rows() != m_training.rows())
    {
        throw DimensionException("Dimension mismatch between training data "
                             "and training labels.");
    }
    AbstractGaussianProcess::set_training_labels(labels);
}

void GPSR::clear_query_points()
{
    AbstractGaussianProcess::clear_query_points();
    m_cov_query_ind = Eigen::MatrixXd();
}

Eigen::MatrixXd const& GPSR::get_training_data() const
{
    return m_training;
}

Eigen::MatrixXd & GPSR::get_training_data()
{
    return m_training;
}

void GPSR::set_inducing_points(Eigen::MatrixXd const& inducing_points)
{
    m_inducing_points = inducing_points;
}

Eigen::MatrixXd const& GPSR::get_induced_points()
{
    return m_inducing_points;
}

void GPSR::solve()
{
    // Check if induced points array is empty
    if(m_inducing_points.rows() == 0)
    {
        throw DimensionException("GPSR: There are no inducing points defined.");
    }

    // Check that dimensions of the induce points is the same as the train
    // and test data.
    unsigned int induced_points_dim = m_inducing_points.cols();
    if(induced_points_dim != (unsigned int)m_training.cols())
    {
        throw DimensionException("GPSR: The dimensionality of the inducing"
                                 " points does not match the expected"
                                 " dimensionality.");
    }

    Eigen::VectorXd mean = get_mean_func()->mean(get_training_data());
    Eigen::MatrixXd k_mm = get_cov_func()->cov(get_induced_points());
    Eigen::MatrixXd k_mn = get_cov_func()->cov(get_induced_points(),get_training_data());

    // Ad a tiny noise to k_mm to keep positive definitiveness.
    double small_noise = 1e-6*k_mm.diagonal().sum();
    k_mm = k_mm + small_noise*Eigen::MatrixXd::Identity(k_mm.rows(),k_mm.cols());

    // Evaluate noise and K hat.
    Eigen::VectorXd noise = get_noise_func()->noise(get_training_data());

    m_k_hat = std::pow(noise[0],2)*k_mm + k_mn*k_mn.transpose();

    Eigen::VectorXd y_m = get_training_labels() - mean;

    Eigen::MatrixXd pseudo_y = k_mn*y_m;

    Eigen::LLT<Eigen::MatrixXd> llt;
    llt.compute(m_k_hat);

    m_alpha = llt.solve(pseudo_y);

    m_k_hat_inv = llt.solve(Eigen::MatrixXd::Identity(k_mm.rows(),k_mm.cols()));
}

GPResult GPSR::inference()
{
    solve();

    Eigen::VectorXd kss(m_query.rows());
    for(int i=0; i<m_query.rows(); ++i)
    {
        kss(i) = get_cov_func()->cov(m_query.row(i)).value();
    }

    Eigen::VectorXd mean_query = get_mean_func()->mean(m_query);
    Eigen::VectorXd noise_query = get_noise_func()->noise(m_query);

    Eigen::MatrixXd km_x_star = get_cov_func()->cov(get_induced_points(),m_query);

    // Compute predictive mean and variance
    GPResult result;
    result.mean = mean_query + (km_x_star.transpose() * m_alpha);  //Rasmussen 2.38
    result.variance = (noise_query[0]*((km_x_star.transpose()*m_k_hat_inv)*km_x_star)).diagonal();

    result.goal_function = evaluate_goal_function(this->get_gp_hyper_params());

    return result;
}

double GPSR::evaluate_goal_function(
        ParametricFunction::ParamVec_t const& gp_params
)
{
    unsigned int mean_init = 0;
    unsigned int mean_end = get_mean_func()->get_nr_of_free_params();

    unsigned int cov_init = mean_end;
    unsigned int cov_end = cov_init +
                            get_cov_func()->get_nr_of_free_params();

    unsigned int noise_init = cov_end;
    unsigned int noise_end = noise_init +
                            get_noise_func()->get_nr_of_free_params();

    //First parameters are from the mean function
    ParametricFunction::ParamVec_t mean_params(gp_params.begin()+mean_init,
                                               gp_params.begin()+mean_end);

    //Next parameters are from the cov function
    ParametricFunction::ParamVec_t cov_params(gp_params.begin()+cov_init,
                                              gp_params.begin()+cov_end);

    //Final parameters are from the noise function
    ParametricFunction::ParamVec_t noise_params(gp_params.begin()+noise_init,
                                                gp_params.begin()+noise_end);

    switch(m_goal_func_type)
    {
        case GF_LML:
        {
            unsigned int N = get_training_data().rows();
            unsigned int m = get_induced_points().rows();

            if(m == 0)
            {
                throw BaseException("No induced points defined.");
            }

            // Calculation based on Tacopig subset of regressors code.
            // The idea is to apply mathematical shortcuts to void the overcome
            // of calculating the inverse of the entire set of regressors.

            Eigen::VectorXd mean = get_mean_func()->mean(get_training_data(),mean_params);
            Eigen::VectorXd noise = get_noise_func()->noise(get_training_data(),noise_params);
            Eigen::MatrixXd k_mm = get_cov_func()->cov(get_induced_points(),cov_params);
            Eigen::MatrixXd k_mn = get_cov_func()->cov(get_induced_points(),get_training_data(),cov_params);

            // Ad a tiny noise to k_mm to keep positive definitiveness.
            double small_noise = 0;//1e-6*k_mm.diagonal().sum();
            k_mm = k_mm + small_noise*Eigen::MatrixXd::Identity(k_mm.rows(),k_mm.cols());

            Eigen::VectorXd y_m = get_training_labels() - mean;

            Eigen::LLT<Eigen::MatrixXd> llt;
            llt.compute(k_mm);

            Eigen::MatrixXd L = llt.matrixL();

            Eigen::MatrixXd V = L.lu().solve(k_mn);

            Eigen::MatrixXd pK = V*V.transpose()+std::pow(noise[0],2)
                                                 *Eigen::MatrixXd::Identity(k_mm.rows(),k_mm.cols());

            Eigen::LLT<Eigen::MatrixXd> llt2;
            llt2.compute(pK);

            Eigen::MatrixXd l_m = llt2.matrixL();

            Eigen::MatrixXd b = l_m.lu().solve(V)*y_m;

            Eigen::MatrixXd y_b = y_m.transpose()*y_m-b.transpose()*b;

            /* For debug
            std::ofstream data("/tmp/debug.dat");
            data << "V" << V << std::endl;
            data << std::endl;
            data << "L" << L << std::endl;
            data << std::endl;
            data << "pK" << pK << std::endl;
            data << std::endl;
            data << "l_m" << l_m << std::endl;
            data << std::endl;
            data << "b" << b << std::endl;
            data << std::endl;
            data << "y_b" << y_b << std::endl;
            */

            // Negative values because is the negative log marginal likelihood
            double lml1 = -l_m.diagonal().array().log().sum();
            double lml2 = -0.5*(N-m)*std::log(std::pow(noise[0],2));
            double lml3 = -0.5*(1/std::pow(noise[0],2))*y_b(0,0);
            double lml4 = -0.5*N*std::log(2*M_PI);

            double lml = lml1+lml2+lml3+lml4;

            return lml;
            break;
        }
        case GF_LOOCV:
        {
            throw(BaseException("Leave One Out Cross Validation not implemented"
                                "yet."));
            break;
        }
    }
    throw(BaseException("Goal function not recognized."));
    return 0;
}

void GPSR::recalculate_covariances()
{
    //Check if cov_func exists
    if(m_cov_func)
    {
//        m_cov_ind_ind

//        m_cov_train_ind
    }
}

} /* gp */
