#ifndef __GP_GP_LARGE_DATA_H__
#define __GP_GP_LARGE_DATA_H__

#include <gp/gp/abstract_gaussian_process.h>

namespace gp
{

/**
 * \brief Gaussian Process Nearest Neighbors. Inherits from a normal GP, but
 *        implements an efficient way of calculating the prediction for a given
 *        point.
 *
 * The covariance matrix is not calculated fully to allow efficient inversion
 * when predicting for new points. This matrix cov_train_train matrix
 * cannot be stored as it is done in GPSimple, because it depends on the
 * query points.
 *
 * The procedure when a new set of query points is received is the following:
 * 1. For every query points: Find its k nearest neighbors (k is determined as
 *    a parameter of GaussianProcessNN).
 * 2. Find a prediction of mean and variance for each query point
 */

class GPLargeData
        :public AbstractGaussianProcess
{
    public:

        GPLargeData();

        GPLargeData(boost::shared_ptr<AbstractCovarianceFunction> cov_func,
                    boost::shared_ptr<AbstractMeanFunction> mean_func,
                    boost::shared_ptr<AbstractNoiseFunction> noise_func
        );

        /**
         * \brief Sets the hyper parameters of the entire gp.
         *
         * \param params set of hyper parameters to use
         */
        void set_gp_hyper_params(
                AbstractCovarianceFunction::ParamVec_t const& params);

        /**
         * \brief Sets the hyper parameters of the covariance function.
         *
         * \param params set of hyper parameters to use
         */
        void set_free_gp_hyper_params(
                AbstractCovarianceFunction::ParamVec_t const& params);

        /**
         * \brief Sets the query points.
         *
         * The covariance between the query points can be provided by the user
         * if it is available.
         *
         * \param query_points the points for which to make predictions
         */
        void set_query_points(Eigen::MatrixXd const& query_points);

        /**
         * \brief Sets the data and labels of the GP to the given values.
         *
         * Each row of the data matrix is assumed to contain a single
         * example  with the same row of the label vector containing the
         * associated label.
         *
         * \param data the new training data to use
         * \param labels the labels corresponding to the data entries
         */
        void set_training_data(
                Eigen::MatrixXd const&  data,
                Eigen::VectorXd const&  labels
        );

        Eigen::MatrixXd & get_training_data();

        /**
         * \brief Appends to given data to the existing data.
         *
         * \param data the data entries to add
         * \param labels the labels of the entries to be added
         */
        void append_training_data(
                Eigen::MatrixXd const&  data,
                Eigen::VectorXd const&  labels
        );

        /**
         * \brief Removes the last data point.
         */
        void remove_last_training_point();

        /**
         * \breif Clears all stored training data.
         */
        void clear_training_data();

        /**
         * \brief Sets the labels of the training data.
         *
         * This method only changes the labels without changing the training
         * data itself.
         *
         * \param labels new labels for the training data
         */
        void set_training_labels(Eigen::VectorXd const& labels);

        /**
         * \brief Clears all current test points.
         */
        void clear_query_points();

        /**
         * \brief Performs inference and returns mean and variance.
         *
         * \param query_points the qyery points to infer values for
         * \return inference results
         */
        GPResult inference();

        double evaluate_goal_function(
                ParametricFunction::ParamVec_t const& gp_params
        );

    private:
        //! Training data
        Eigen::MatrixXd     m_training;

        //! Number of nearest neighbors per query point
        unsigned int        m_n_nearest_neighbors;

        //! Number of points used to calculate goal function
        unsigned int        m_n_goal_func;
};

} /* gp */

#endif /* __GP_GP_LARGE_DATA_H__ */
