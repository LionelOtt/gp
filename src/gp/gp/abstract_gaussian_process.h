#ifndef __GP_ABSTRACT_GAUSSIAN_PROCESS_H__
#define __GP_ABSTRACT_GAUSSIAN_PROCESS_H__

#include <vector>

#include <Eigen/Core>
#include <Eigen/Dense>
#include <boost/shared_ptr.hpp>
#include <boost/make_shared.hpp>

#include <gp/gp/error.hpp>
#include <gp/gp/gp_result.h>
#include <gp/gp/optimisers/optimiser_params.h>
#include <gp/covariance_functions/abstract_covariance_function.h>
#include <gp/mean_functions/abstract_mean_function.h>
#include <gp/noise_functions/abstract_noise_function.h>

namespace gp
{

enum GoalFunctionType
{
    GF_LML = 0,
    GF_LOOCV
};

/**
 * \brief Abstract Gaussian Process is the base class for all types of gaussian
 * processes.
 *
 * - CovFunc is the covariance function used by the GP
 * - MeanFunc is the mean function used by the GP
 *
 * The form of storage of information and handling of this data depends on each
 * type of GP.
 *
 * The main function to be implemented by each type is "inference",
 * i.e. calculating the predictive distribution over a set of query points.
 * Depending on the type of GP, each one does this differently, for example,
 *
 * Normal storage of observations as point coordintes:
 * Large Data GP only uses nearest neighbors approach to select training points
 * and only invert a subset of the large covariance matrix.
 * GP_simple stores the covariance matrix and only recalculates it when the
 * training data is modified (If the number of data points is greater that a
 * threshold the process of inverting the covariance matrix will become very
 * slow).
 *
 * Generic observation GPs use observations over lines, paths or surfaces to
 * handle observations over continuous domains.
 *
 */

class AbstractGaussianProcess
{
    public:

        AbstractGaussianProcess();

        AbstractGaussianProcess(
                        boost::shared_ptr<AbstractCovarianceFunction> cov_func,
                        boost::shared_ptr<AbstractMeanFunction> mean_func,
                        boost::shared_ptr<AbstractNoiseFunction> noise_func
        );

        virtual ~AbstractGaussianProcess() {}

        /**
         * \brief Sets the hyper parameters of the entire gp.
         *
         * These hyper-parameters include the parameters for the mean function,
         * covariance function and noise function.
         *
         * Is virtual to allow other GPs to change behaviour after changing
         * hyperparams, such as recalculating the covariance matrix. The input
         * array should contain all of these parameters concatenated.
         * [mean_params,cov_params,noise_params]
         *
         * \param params Set of hyper parameters to be used.
         */
        virtual void set_gp_hyper_params(
                ParametricFunction::ParamVec_t const& params);

        ParametricFunction::ParamVec_t get_gp_hyper_params();

        /**
         * \brief Sets the free hyper parameters of the entire gp.
         *
         * These hyper-parameters include the parameters for the mean function,
         * covariance function and noise function.
         *
         * Is virtual to allow other GPs to change behaviour after changing
         * hyperparams, such as recalculating the covariance matrix. The input
         * array should contain all of these parameters concatenated.
         * [mean_params,cov_params,noise_params].
         *
         * \param params Set of hyper parameters to be used.
         */
        virtual void set_free_gp_hyper_params(
                ParametricFunction::ParamVec_t const& params);

        ParametricFunction::ParamVec_t get_free_gp_hyper_params();

        /**
         * \brief Sets the hyper parameters of the gp's covariance function.
         *
         * Is virtual to allow other GPs to change behaviour after changing
         * hyperparams, such as recalculating the covariance matrix.
         *
         * \param params Set of hyper parameters to be used.
         */
        virtual void set_cov_hyper_params(
                ParametricFunction::ParamVec_t const& params);

        /**
         * \brief Sets the hyper parameters of the gp's mean function.
         *
         * Is virtual to allow other GPs to change behaviour after changing
         * hyperparams, such as recalculating the covariance matrix.
         *
         * \param params Set of hyper parameters to be used.
         */
        virtual void set_mean_hyper_params(
                ParametricFunction::ParamVec_t const& params);

        /**
         * \brief Sets the hyper parameters of the gp's noise function.
         *
         * Is virtual to allow other GPs to change behaviour after changing
         * hyperparams, such as recalculating the covariance matrix.
         *
         * \param params Set of hyper parameters to be used.
         */
        virtual void set_noise_hyper_params(
                ParametricFunction::ParamVec_t const& params);

        /**
         * \brief Sets the query points.
         *
         * The covariance between the query points can be provided by the user
         * if it is available.
         *
         * \param query_points the points for which to make predictions
         * \param covariance covariance matrix of the query data
         */
        void set_query_points(Eigen::MatrixXd const&  query_points);

        Eigen::MatrixXd & get_query_points();

        /**
         * \brief Clears all current test points.
         */
        virtual void clear_query_points();

        /**
         * \brief Sets the labels of the training data.
         *
         * This method only changes the labels without changing the training
         * data itself.
         *
         * \param labels new labels for the training data
         */
        virtual void set_training_labels(Eigen::VectorXd const& labels);

        Eigen::VectorXd const& get_training_labels() const;

        Eigen::VectorXd & get_training_labels();

    public:
        /**
         * \brief Clears all stored training data.
         *
         * Because each GP stores training data of different type, each GP
         * knows how to clear its own training data.
         * For example, integral line GPs store data as lines not as points.
         */
        virtual void clear_training_data() = 0;

        /**
         * \brief Performs inference and returns mean and variance.
         *
         * \param query_points the qyery points to infer values for
         * \return inference results
         */        
        virtual GPResult inference() = 0;

        GPResult inference(Eigen::MatrixXd const& query_points);

        /**
         * \brief Evaluates a goal function depending on the data, and the
         * hyperparameters.
         *
         * The parameters include the mean, covariance and noise function.
         * If no parameters are provided, then this function is called using
         * the ones stored in the gp structure.
         *
         * \param gp_params Parameter vector of the covariance function
         * \return goal function score
         */
        virtual double evaluate_goal_function(
                ParametricFunction::ParamVec_t const& gp_params) = 0;

        double evaluate_goal_function();

        virtual OptimiserParams get_optimiser_params();

    public:
        boost::shared_ptr<AbstractCovarianceFunction> get_cov_func();

        boost::shared_ptr<AbstractMeanFunction> get_mean_func();

        boost::shared_ptr<AbstractNoiseFunction> get_noise_func();

        void set_cov_func(
                boost::shared_ptr<AbstractCovarianceFunction> cov_func);

        void set_mean_func(
                boost::shared_ptr<AbstractMeanFunction> mean_func);

        void set_noise_func(
                boost::shared_ptr<AbstractNoiseFunction> noise_func);

    protected:
        boost::shared_ptr<AbstractCovarianceFunction> m_cov_func;

        boost::shared_ptr<AbstractMeanFunction> m_mean_func;

        boost::shared_ptr<AbstractNoiseFunction> m_noise_func;

        //! Labels for the training data
        Eigen::VectorXd  m_labels;

        //! Query points
        Eigen::MatrixXd  m_query;

        //! Type of goal function to be optimised
        GoalFunctionType m_goal_func_type;
};

} /* gp */

#endif /* __GP_ABSTRACT_GAUSSIAN_PROCESS_H__ */
