#include <gp/gp/abstract_gaussian_process.h>

namespace gp
{

class {
public:
    template<typename T>
    operator boost::shared_ptr<T>() { return boost::shared_ptr<T>(); }
} nullPtr;

AbstractGaussianProcess::AbstractGaussianProcess()
    :m_cov_func(nullPtr)
    ,m_mean_func(nullPtr)
    ,m_noise_func(nullPtr)
    ,m_goal_func_type(GF_LML)
{

}

AbstractGaussianProcess::AbstractGaussianProcess(
        boost::shared_ptr<AbstractCovarianceFunction> cov_func,
        boost::shared_ptr<AbstractMeanFunction> mean_func,
        boost::shared_ptr<AbstractNoiseFunction> noise_func
)
    :m_cov_func(cov_func)
    ,m_mean_func(mean_func)
    ,m_noise_func(noise_func)
    ,m_goal_func_type(GF_LML)
{

}

void AbstractGaussianProcess::set_gp_hyper_params(
        ParametricFunction::ParamVec_t const& params
)
{
    //Check if the size of all parameters match the expected by the sizes
    //of the mean function hyper params, the covariance function hyperparams
    //and the noise hyper params.
    if(params.size() != get_gp_hyper_params().size())
    {
        throw(ParameterException("set_gp_hyper_params: Parameters array is not of"
                             "expected size."));
    }

    //Check if functions exist
    if(m_cov_func && m_mean_func && m_noise_func)
    {
        unsigned int mean_init = 0;
        unsigned int mean_end = get_mean_func()->get_nr_of_params();

        unsigned int cov_init = mean_end;
        unsigned int cov_end = cov_init + get_cov_func()->get_nr_of_params();

        unsigned int noise_init = cov_end;
        unsigned int noise_end = noise_init +
                                        get_noise_func()->get_nr_of_params();

        //First parameters are from the mean function
        ParametricFunction::ParamVec_t mean_params(params.begin()+mean_init,
                                                   params.begin()+mean_end);

        //Next parameters are from the cov function
        ParametricFunction::ParamVec_t cov_params(params.begin()+cov_init,
                                                  params.begin()+cov_end);

        //Final parameters are from the noise function
        ParametricFunction::ParamVec_t noise_params(params.begin()+noise_init,
                                                    params.begin()+noise_end);

        get_mean_func()->set_hyper_params(mean_params);
        get_cov_func()->set_hyper_params(cov_params);
        get_noise_func()->set_hyper_params(noise_params);
    }
    else
    {
        throw(ParameterException("Trying to set parameters of a GP that is not"
                        "fully determined."));
    }
}

ParametricFunction::ParamVec_t
            AbstractGaussianProcess::get_gp_hyper_params()
{
    ParametricFunction::ParamVec_t all_params;

    //Check if cov_func exists
    if(m_cov_func && m_mean_func && m_noise_func)
    {
        ParametricFunction::ParamVec_t mean_function_params =
                get_mean_func()->get_hyper_params();
        all_params.insert(all_params.end(),mean_function_params.begin(),
                                           mean_function_params.end());

        ParametricFunction::ParamVec_t cov_function_params =
                get_cov_func()->get_hyper_params();
        all_params.insert(all_params.end(), cov_function_params.begin(),
                                            cov_function_params.end());

        ParametricFunction::ParamVec_t noise_function_params =
                get_noise_func()->get_hyper_params();
        all_params.insert(all_params.end(), noise_function_params.begin(),
                                            noise_function_params.end());
    }
    else
    {
        throw(ParameterException("Requesting parameters of a GP that is not fully"
                        "determined."));
    }

    return all_params;
}

void AbstractGaussianProcess::set_free_gp_hyper_params(
        AbstractCovarianceFunction::ParamVec_t const& params
)
{
    //Check if the size of all parameters match the expected by the sizes
    //of the mean function hyper params, the covariance function hyperparams
    //and the noise hyper params.
    if(params.size() != get_free_gp_hyper_params().size())
    {
        throw(ParameterException("set_free_hyper_params: Free parameters array is"
                             "not of expected size."));
    }

    //Check if functions exist
    if(m_cov_func && m_mean_func && m_noise_func)
    {
        unsigned int mean_init = 0;
        unsigned int mean_end = get_mean_func()->get_nr_of_free_params();

        unsigned int cov_init = mean_end;
        unsigned int cov_end = cov_init +
                                 get_cov_func()->get_nr_of_free_params();

        unsigned int noise_init = cov_end;
        unsigned int noise_end = noise_init +
                               get_noise_func()->get_nr_of_free_params();

        //First parameters are from the mean function
        ParametricFunction::ParamVec_t mean_params(params.begin()+mean_init,
                                                   params.begin()+mean_end);

        //Next parameters are from the cov function
        ParametricFunction::ParamVec_t cov_params(params.begin()+cov_init,
                                                  params.begin()+cov_end);

        //Final parameters are from the noise function
        ParametricFunction::ParamVec_t noise_params(params.begin()+noise_init,
                                                    params.begin()+noise_end);

        get_mean_func()->set_free_hyper_params(mean_params);
        get_cov_func()->set_free_hyper_params(cov_params);
        get_noise_func()->set_free_hyper_params(noise_params);
    }
    else
    {
        throw(ParameterException("Trying to set parameters of a GP that is not"
                        "fully determined."));
    }
}

AbstractCovarianceFunction::ParamVec_t
            AbstractGaussianProcess::get_free_gp_hyper_params()
{
    ParametricFunction::ParamVec_t all_free_params;

    //Check if cov_func exists
    if(m_cov_func && m_mean_func && m_noise_func)
    {
        ParametricFunction::ParamVec_t mean_function_params =
                get_mean_func()->get_free_hyper_params();
        all_free_params.insert(all_free_params.end(),
                               mean_function_params.begin(),
                               mean_function_params.end());

        ParametricFunction::ParamVec_t cov_function_params =
                get_cov_func()->get_free_hyper_params();
        all_free_params.insert(all_free_params.end(),
                               cov_function_params.begin(),
                               cov_function_params.end());

        ParametricFunction::ParamVec_t noise_function_params =
                get_noise_func()->get_free_hyper_params();
        all_free_params.insert(all_free_params.end(),
                               noise_function_params.begin(),
                               noise_function_params.end());
    }
    else
    {
        throw(ParameterException("Requesting parameters of a GP that is not fully"
                        "determined."));
    }

    return all_free_params;
}

void AbstractGaussianProcess::set_cov_hyper_params(
        ParametricFunction::ParamVec_t const& params)
{
    if(m_cov_func)
    {
        get_cov_func()->set_hyper_params(params);
    }
    else
    {
        throw(ParameterException("Setting the parameters of an undetermined "
                             "covariance funcion."));
    }
}

void AbstractGaussianProcess::set_mean_hyper_params(
        ParametricFunction::ParamVec_t const& params)
{
    if(m_mean_func)
    {
        get_mean_func()->set_hyper_params(params);
    }
    else
    {
        throw(ParameterException("Setting the parameters of an undetermined "
                             "mean funcion."));
    }
}

void AbstractGaussianProcess::set_noise_hyper_params(
        ParametricFunction::ParamVec_t const& params)
{
    if(m_noise_func)
    {
        get_noise_func()->set_hyper_params(params);
    }
    else
    {
        throw(ParameterException("Setting the parameters of an undetermined "
                             "noise funcion."));
    }
}

void AbstractGaussianProcess::set_query_points(
        Eigen::MatrixXd const&          query_points
)
{
    //Directly assign query point matrix
    m_query = query_points;
}

Eigen::MatrixXd & AbstractGaussianProcess::get_query_points()
{
    return m_query;
}

void AbstractGaussianProcess::clear_query_points()
{
    m_query = Eigen::MatrixXd();
}

void AbstractGaussianProcess::set_training_labels(Eigen::VectorXd const& labels)
{
    m_labels = labels;
}

Eigen::VectorXd const& AbstractGaussianProcess::get_training_labels() const
{
    return m_labels;
}

Eigen::VectorXd & AbstractGaussianProcess::get_training_labels()
{
    return m_labels;
}

GPResult AbstractGaussianProcess::inference(Eigen::MatrixXd const& query_points)
{
    set_query_points(query_points);
    return inference();
}

double AbstractGaussianProcess::evaluate_goal_function()
{

    return evaluate_goal_function(get_gp_hyper_params());
}

OptimiserParams AbstractGaussianProcess::get_optimiser_params()
{
    OptimiserParams opt_params;
    if(m_cov_func && m_mean_func && m_noise_func)
    {
        unsigned int unknowns = m_cov_func->get_nr_of_free_params() +
                                m_mean_func->get_nr_of_free_params() +
                                m_noise_func->get_nr_of_free_params();
        opt_params.set_number_of_unknowns(unknowns);
    } else
    {
        throw(ParameterException("Requesting optimiser parameters of a GP that"
                             "is not fully determined."));
    }
    return opt_params;
}

boost::shared_ptr<AbstractCovarianceFunction>
    AbstractGaussianProcess::get_cov_func()
{
    return m_cov_func;
}

boost::shared_ptr<AbstractMeanFunction> AbstractGaussianProcess::get_mean_func()
{
    return m_mean_func;
}

boost::shared_ptr<AbstractNoiseFunction>
                                AbstractGaussianProcess::get_noise_func()
{
    return m_noise_func;
}

void AbstractGaussianProcess::set_cov_func(
        boost::shared_ptr<AbstractCovarianceFunction> cov_func)
{
    m_cov_func = cov_func;
}

void AbstractGaussianProcess::set_mean_func(
        boost::shared_ptr<AbstractMeanFunction> mean_func)
{
    m_mean_func = mean_func;
}

void AbstractGaussianProcess::set_noise_func(
        boost::shared_ptr<AbstractNoiseFunction> noise_func)
{
    m_noise_func = noise_func;
}

} /* gp */
