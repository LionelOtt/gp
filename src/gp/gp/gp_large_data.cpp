#include <gp/gp/gp_large_data.h>

namespace gp
{

GPLargeData::GPLargeData()
    :AbstractGaussianProcess()
    ,m_n_nearest_neighbors(300)
    ,m_n_goal_func(300)
{

}

GPLargeData::GPLargeData(
        boost::shared_ptr<AbstractCovarianceFunction> cov_func,
        boost::shared_ptr<AbstractMeanFunction> mean_func,
        boost::shared_ptr<AbstractNoiseFunction> noise_func
)
    :AbstractGaussianProcess(cov_func,mean_func,noise_func)
    ,m_n_nearest_neighbors(300)
    ,m_n_goal_func(300)
{

}

void GPLargeData::set_gp_hyper_params(
        AbstractCovarianceFunction::ParamVec_t const& params
)
{
    AbstractGaussianProcess::set_gp_hyper_params(params);
}

void GPLargeData::set_free_gp_hyper_params(
        AbstractCovarianceFunction::ParamVec_t const& params
)
{
    AbstractGaussianProcess::set_free_gp_hyper_params(params);
}

void GPLargeData::set_training_data(
        Eigen::MatrixXd const&          data,
        Eigen::VectorXd const&          labels
)
{
    if(data.rows() != labels.rows())
    {
        throw DimensionException("Row count of data and labels does not match");
    }
    m_training = data;
    m_labels = labels;
}

Eigen::MatrixXd & GPLargeData::get_training_data()
{
    return m_training;
}

void GPLargeData::clear_training_data()
{
    m_training = Eigen::MatrixXd();
    m_labels = Eigen::VectorXd();
}


void GPLargeData::append_training_data(
        Eigen::MatrixXd const&          data,
        Eigen::VectorXd const&          labels
)
{
    if(data.rows() != labels.rows())
    {
        throw DimensionException("Row count of new data does not match");
    }
    if(m_training.rows() > 0 && data.cols() != m_training.cols())
    {
        throw DimensionException("Dimensionality of old and new data"
                             "does not match");
    }

    int new_rowcount = m_training.rows() + data.rows();

    // Resize data storage
    if(m_training.cols() == 0)
    {
        m_training.conservativeResize(data.rows(), data.cols());
    }
    else
    {
        m_training.conservativeResize(new_rowcount, m_training.cols());
    }
    m_labels.conservativeResize(new_rowcount);

    // Append new data at the end
    m_training.bottomRows(data.rows()) = data;
    m_labels.tail(labels.rows()) = labels;
}


void GPLargeData::remove_last_training_point()
{
    // Check if there is something to remove
    if(m_training.rows() == 0 || m_labels.rows() == 0)
    {
        throw DimensionException("There is no old data to remove");
    }

    // Remove data and label
    m_training.conservativeResize(m_training.rows()-1, m_training.cols());
    m_labels.conservativeResize(m_labels.rows()-1);
}


void GPLargeData::set_training_labels(Eigen::VectorXd const& labels)
{
    if(labels.rows() != m_training.rows())
    {
        throw DimensionException("Dimension mismatch between training data "
                             "and training labels.");
    }
    AbstractGaussianProcess::set_training_labels(labels);
}

void GPLargeData::set_query_points(Eigen::MatrixXd const& query_points)
{
    AbstractGaussianProcess::set_query_points(query_points);
    //TODO Calculate nearest neighbor labels for each query point.
}

void GPLargeData::clear_query_points()
{
    m_query = Eigen::MatrixXd();
    //TODO Clear matrix that stores nearest neighbors
}

GPResult GPLargeData::inference()
{
    //Essentially do inference for every point in the query domain.
    //For every query point find the nearest neighbors and use only them to
    //fill the gram matrix.
    GPResult result;
    result.mean.resize(m_query.rows());
    result.variance.resize(m_query.rows());

    for(unsigned int i = 0; i < (unsigned int)m_query.rows(); i++)
    {
        Eigen::MatrixXd current_query = m_query.row(i);
        // Calculate covariance values with the current query of all the
        // training datapoints.
        Eigen::VectorXd cov_values = m_cov_func->cov(m_training, current_query);

        // Create a vector of indexes
        std::vector<unsigned int> indexes(cov_values.size());
        // Fill the vector with consecutive values
        for(unsigned int i = 0; i < indexes.size(); i++)
        {
            indexes[i] = i;
        }
        // Sort the indexes depending on the covariance values
        std::sort(
            begin(indexes), end(indexes),
            [&](unsigned int a, unsigned int b)
                    { return cov_values[a] > cov_values[b]; }
        );

        // Find the number of relevant examples
        unsigned int n_of_datapoints = std::min(m_n_nearest_neighbors,
                                           (unsigned int)m_training.rows());

        // Create a matrix with the data of the points to be used
        Eigen::MatrixXd relevant_training_data(n_of_datapoints,
                                               m_training.cols());
        Eigen::VectorXd relevant_training_labels(n_of_datapoints);
        // Fill this matrix with the most relevant points
        for(unsigned int j = 0 ; j < n_of_datapoints; j++)
        {
            relevant_training_data.row(j) = m_training.row(indexes[j]);
            relevant_training_labels(j) = get_training_labels().coeff(indexes[j]);
        }

        Eigen::MatrixXd kss = get_cov_func()->cov(current_query);

        Eigen::MatrixXd ks = get_cov_func()->cov(relevant_training_data,
                                                 current_query);

        Eigen::VectorXd m = get_mean_func()->mean(relevant_training_data);

        Eigen::VectorXd noise_val = get_noise_func()->noise(relevant_training_data);

        Eigen::MatrixXd L =  m_cov_func->cov(relevant_training_data) +
                noise_val[0] * noise_val[0] *
                Eigen::MatrixXd::Identity(relevant_training_data.rows(),
                                          relevant_training_data.rows());
        L = L.selfadjointView<Eigen::Upper>().llt().matrixU();
        Eigen::VectorXd alpha = L.triangularView<Eigen::Upper>()
            .transpose().solve(relevant_training_labels - m);
        L.triangularView<Eigen::Upper>().solveInPlace(alpha);

        Eigen::VectorXd ms = get_mean_func()->mean(current_query);
        result.mean.row(i) = ms + (ks.transpose() * alpha);
        Eigen::MatrixXd v = L.triangularView<Eigen::Upper>().
                            transpose().solve(ks);

        result.variance.row(i) = kss -
                        v.array().square().colwise().sum().transpose().matrix();
    }

    return result;
}

double GPLargeData::evaluate_goal_function(
        ParametricFunction::ParamVec_t const& gp_params)
{
    //Evaluate only with the first acquired datapoints.

    unsigned int n_of_datapoints = std::min(m_n_goal_func,
                                            (unsigned int)m_training.rows());

    // Create a matrix with the data of the points to be used
    Eigen::MatrixXd relevant_training_data(n_of_datapoints,
                                           m_training.cols());

    // Fill this matrix with first acquired datapoints
    for(unsigned int i = 0 ; i < n_of_datapoints; i++)
    {
        relevant_training_data.row(i) = m_training.row(i);
    }

    unsigned int mean_init = 0;
    unsigned int mean_end = get_mean_func()->get_nr_of_free_params();

    unsigned int cov_init = mean_end;
    unsigned int cov_end = cov_init +
                             get_cov_func()->get_nr_of_free_params();

    unsigned int noise_init = cov_end;
    unsigned int noise_end = noise_init +
                           get_noise_func()->get_nr_of_free_params();

    //First parameters are from the mean function
    ParametricFunction::ParamVec_t mean_params(gp_params.begin()+mean_init,
                                               gp_params.begin()+mean_end);

    //Next parameters are from the cov function
    ParametricFunction::ParamVec_t cov_params(gp_params.begin()+cov_init,
                                              gp_params.begin()+cov_end);

    //Final parameters are from the noise function
    ParametricFunction::ParamVec_t noise_params(gp_params.begin()+noise_init,
                                                gp_params.begin()+noise_end);


    switch(m_goal_func_type)
    {
        case GF_LML:
        {
            unsigned int N = relevant_training_data.rows();

            Eigen::MatrixXd K = get_cov_func()->cov(relevant_training_data,
                                                    cov_params);

            Eigen::VectorXd noise_val = get_noise_func()->noise(
                                                    relevant_training_data,
                                                    noise_params);

            Eigen::VectorXd m = get_mean_func()->mean(relevant_training_data,
                                                      mean_params);

            Eigen::MatrixXd L = (K +
                    (noise_val[0]*noise_val[0])*Eigen::MatrixXd::Identity(N,N));

            Eigen::VectorXd y = get_training_labels() - m;

            Eigen::LLT<Eigen::MatrixXd> llt;
            llt.compute(L);

            Eigen::MatrixXd alpha;
            alpha = llt.solve(y);

            double llt_diagonal_sum = 0;

            for(unsigned int i = 0;
                i < (unsigned int)llt.matrixLLT().rows();
                i++)
            {
                llt_diagonal_sum += log(llt.matrixLLT()(i,i));
            }

            Eigen::MatrixXd lml1 = -0.5*((y.transpose())*alpha);
            double lml2 = - llt_diagonal_sum;
            double lml3 = -0.5*N*log(2*M_PI);
            double lml = lml1(0,0)+lml2+lml3;

            return lml;
            break;
        }
        case GF_LOOCV:
        {
            throw(BaseException("Leave One Out Cross Validation not implemented"
                            "yet."));
            break;
        }
    }
    throw(BaseException("Goal function not recognized."));
    return 0;

}


} /* gp */
