#ifndef __GP_GP_RESULT_H__
#define __GP_GP_RESULT_H__


#include <Eigen/Core>


namespace gp
{
    
/**
 * \brief Stores the inference result of a Gaussian process.
 */
class GPResult
{
    public:
        //! The predicted mean values
        Eigen::VectorXd mean;
        //! The variances associated to the predicted mean
        Eigen::VectorXd variance;
        //! The output of the goal function
        double          goal_function;
};

} /* gp */

#endif /* __GP_GP_RESULT_H__ */
