#ifndef __GP_OPTIMISER_PARAMS_H__
#define __GP_OPTIMISER_PARAMS_H__

#include <tuple>
#include <vector>

namespace gp
{
    
/**
 * \brief Parameters given to the optimiser that define the goal function.
 *
 * This class serves mainly as a structure, used by the optimiser and filled by
 * the gp object that defines the problem to be optimised, i.e. the
 * number of parameters to be optimised, restrictions, presence of gradients
 * and so on.
 */
class OptimiserParams
{
    public:        
        /**
         * \brief Enumeration of the type of goal
         */
        enum GoalType
        {
            MAXIMISE = 0,
            MINIMISE
        };

        /**
         * \brief Enumeration of the possible optimisation constraints.
         */
        enum Constraint
        {
            LESS_THEN = 1,
            LESS_OR_EQUAL_THEN,
            GREATHER_THEN,
            GREATHER_OR_EQUAL_THEN,
            MAXIMUM_VALUE,
            MINIMUM_VALUE,
            COUNT
        };

        /**
         * \brief Typedef for constraint storage structure
         * First parameter is the index associated to the free parameters, and
         * must be smaller than the number of unknowns.
         * The second parameter is the type of constraint, following the enum.
         * The third parameter is the value assigned for the constraint.
         */

        typedef std::vector<std::tuple<unsigned int, Constraint, double> >
                                                            ConstraintsTuple_t;

    public:

        OptimiserParams();

        ~OptimiserParams();

        void add_constraint(unsigned int            param_idx,
                            Constraint              constraint,
                            double                  value
        );

        /**
         * \brief Modify an existing constraint.
         *
         * \param constrain_idx The index of the constraint to be modified.
         * \param param_idx the index of the parameter for which to set a constraint
         * \param constraint they type of constraint to set
         * \param value the value of the constraint
         */
        void modify_constraint(
                unsigned int            constrain_idx,
                unsigned int            param_idx,
                Constraint              constraint,
                double                  value
        );

        /**
         * \brief Returns all stored constraints.
         *
         * \return all the stored constraints
         */
        ConstraintsTuple_t get_constraints() const;

        void set_number_of_unknowns(unsigned int n_unknowns);

        unsigned int get_number_of_unknowns();

        GoalType get_goal_type();

        void set_goal_type(GoalType goal_type);

        void reset();

        OptimiserParams merge(OptimiserParams const& other) const;

    private:
        //! Storage for the optimisation constraints
        ConstraintsTuple_t  m_constraints;

        //! Number of unknowns
        unsigned int        m_number_of_unknowns;

        //! Known derivatives
        bool                m_known_derivatives;

        //! Goal Type
        GoalType            m_goal_type;
};

} /* gp */

#endif /* __GP_OPTIMISER_PARAMS_H__ */
