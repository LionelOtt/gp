#include <gp/gp/optimisers/optimiser_params.h>
#include <gp/gp/error.hpp>

namespace gp
{

OptimiserParams::OptimiserParams()
{
    reset();
}

OptimiserParams::~OptimiserParams()
{

}

void OptimiserParams::add_constraint(
        unsigned int            param_idx,
        Constraint              constraint,
        double                  value
)
{
    if(param_idx >= get_number_of_unknowns())
    {
        throw DimensionException("OptimiserParams: Constraint associated to"
                             "non existing parameter.");
    }

    m_constraints.push_back(std::make_tuple(param_idx,constraint,value));
}

void OptimiserParams::modify_constraint(
        unsigned int            constrain_idx,
        unsigned int            param_idx,
        Constraint              constraint,
        double                  value
)
{
    if(constrain_idx < m_constraints.size())
    {
        throw DimensionException("OptimiserParams::modify_constraint :"
                             "Trying to modify non existing constrain.");
    }

    m_constraints[constrain_idx] = std::make_tuple(param_idx,constraint,value);
}

OptimiserParams::ConstraintsTuple_t OptimiserParams::get_constraints() const
{
    return m_constraints;
}

void OptimiserParams::set_number_of_unknowns(unsigned int n_unknowns)
{
    m_number_of_unknowns = n_unknowns;
}

unsigned int OptimiserParams::get_number_of_unknowns()
{
    return m_number_of_unknowns;
}

OptimiserParams::GoalType OptimiserParams::get_goal_type()
{
    return m_goal_type;
}

void OptimiserParams::set_goal_type(GoalType goal_type)
{
    m_goal_type = goal_type;
}

void OptimiserParams::reset()
{
    m_number_of_unknowns = 0;
    m_constraints.clear();
    m_known_derivatives = false;
    m_goal_type = MAXIMISE;
}

} /* gp */
