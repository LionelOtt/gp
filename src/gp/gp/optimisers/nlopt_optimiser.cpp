#include <gp/gp/optimisers/nlopt_optimiser.h>
#include <iomanip>

namespace gp
{


//! Callback Function.
//! Very important function because it is a trampolyn function so that the
//! optimiser library can call a member function of NLoptOptimiser to be
//! optimised, and use information from the class.

double callback(
        std::vector<double> const&      x,
        std::vector<double> &           grad,
        void *                          func_ptr
)
{
    typename NLoptOptimiser::OptFunc_t func =
            *static_cast<typename NLoptOptimiser::OptFunc_t*>(func_ptr);
    return func(x, grad);
}

NLoptOptimiser::NLoptOptimiser()
    :m_internal_grad(true)
    ,m_nlopt_dim(0)
    ,m_max_iterations(300)
    ,m_iterations(0)
    ,m_verbose(false)
{
    m_algorithm_id = nlopt::LN_COBYLA;
}

void NLoptOptimiser::optimise(AbstractGaussianProcess & gp)
{
    //Fill the information for the optimiser params.
    OptimiserParams optimiser_params = gp.get_optimiser_params();

    //problem sizes
    m_nlopt_dim = optimiser_params.get_number_of_unknowns();

    m_opt = boost::shared_ptr<nlopt::opt>(
                new nlopt::opt(m_algorithm_id,(unsigned int)m_nlopt_dim));

    sol_initial.clear();

    sol_initial = gp.get_free_gp_hyper_params();

    OptFunc_t func = boost::bind(&NLoptOptimiser::objective_func,
                                 this, _1, _2, boost::ref(gp));

    m_opt->set_max_objective(callback, &func);

    m_opt->set_xtol_rel(1e-4);
    m_opt->set_ftol_abs(1e-2);

    m_opt->set_maxeval(m_max_iterations);

    double minf;

    nlopt::result result = m_opt->optimize(sol_initial, minf);

    if (result < 0)
    {
        throw OptimiserException("NLopt: Optimisation Failed");
    }

    gp.set_free_gp_hyper_params(sol_initial);

    std::cout<<"Found result: f(";
    std::vector<double> optimal_params = gp.get_gp_hyper_params();

    for(unsigned int i = 0; i < optimal_params.size(); i++)
    {
        if(i == (optimal_params.size() - 1))
        {
            std::cout<<optimal_params[i]<<") = ";
        }
        else
        {
            std::cout<<optimal_params[i]<<",";
        }
    }
    std::cout<<minf<<std::endl;
}


double NLoptOptimiser::objective_func(const std::vector<double> &x,
                                      std::vector<double>       &grad,
                                      AbstractGaussianProcess   &gp
)
{
    m_iterations++;
    //transform x to params
    std::vector<double> hyper_params = gp.get_free_gp_hyper_params();
    if(hyper_params.size() == m_nlopt_dim)
    {
        double value = gp.evaluate_goal_function(x);

        if(m_verbose)
        {
            if(m_iterations == 1)
            {
                std::cout<<"Iterations\t\t"<<"Params\t\t\t"<<"Objective"<<std::endl;
            }
            std::cout<<m_iterations<<"\t";
            for(unsigned int k = 0; k < x.size(); k++)
            {
                if( k == 0 )
                {
                    std::cout<<std::setprecision(3)<<"("<<x[k];
                }
                else if( k == x.size()-1 )
                {
                    std::cout<<std::setprecision(3)<<","<<x[k]<<")";
                }
                else
                {
                    std::cout<<std::setprecision(3)<<","<<x[k];
                }
            }
            std::cout << std::fixed;
            std::cout<<std::setprecision(5)<<"\t"<<value<<std::endl;
        }
        return value;
    } else
    {
        throw DimensionException("NLoptOptimiser: Mismach of parameters to be "
                             " evaluated in goal function.");
    }
}


std::string NLoptOptimiser::get_name() const
{
    return "NLopt";
}


void NLoptOptimiser::set_verbose(bool verbose)
{
    m_verbose = verbose;
}

void NLoptOptimiser::set_max_iterations(unsigned int max_iterations)
{
    m_max_iterations = max_iterations;
}

void NLoptOptimiser::set_grad_calculation_internal(
        bool calc_grad_internally)
{
    m_internal_grad = calc_grad_internally;
}

} /* gp */

