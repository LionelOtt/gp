#include <gp/gp/gp_simple.h>

namespace gp
{

class {
public:
    template<typename T>
    operator boost::shared_ptr<T>() { return boost::shared_ptr<T>(); }
} nullPtr;

GPSimple::GPSimple()
    :AbstractGaussianProcess(nullPtr,nullPtr,nullPtr)
{

}

GPSimple::GPSimple(
        boost::shared_ptr<AbstractCovarianceFunction> cov_func,
        boost::shared_ptr<AbstractMeanFunction> mean_func,
        boost::shared_ptr<AbstractNoiseFunction> noise_func
)
    :AbstractGaussianProcess(cov_func,mean_func,noise_func)
{

}

void GPSimple::set_gp_hyper_params(
        ParametricFunction::ParamVec_t const& params
)
{
    //Only do something if parameters have changed.
    if(get_gp_hyper_params() != params)
    {
        AbstractGaussianProcess::set_gp_hyper_params(params);
        //Recalculates covariances to achieve faster prediction.
        recalculate_cov_train_train();
        recalculate_cov_query_train();
        recalculate_cov_query_query();
    }
}


void GPSimple::set_free_gp_hyper_params(
        ParametricFunction::ParamVec_t const& params
)
{
    if(get_free_gp_hyper_params() != params)
    {
        AbstractGaussianProcess::set_free_gp_hyper_params(params);
        //Recalculates covariances to achieve faster prediction.
        recalculate_cov_train_train();
        recalculate_cov_query_train();
        recalculate_cov_query_query();
    }

}

void GPSimple::set_training_data(
        Eigen::MatrixXd const&          data,
        Eigen::VectorXd const&          labels
)
{
    if(data.rows() != labels.rows())
    {
        throw DimensionException("Row count of data and labels does not match");
    }
    m_training = data;
    m_labels = labels;

    //Check if cov_func exists
    if(get_cov_func())
    {
        recalculate_cov_train_train();

        if(get_query_points().rows() > 0 && get_query_points().cols() ==
                                                            m_training.cols())
        {
            recalculate_cov_query_train();
        }
    }
}

void GPSimple::clear_training_data()
{
    m_training = Eigen::MatrixXd();
    m_labels = Eigen::VectorXd();
    m_cov_train_train = Eigen::MatrixXd();
    m_cholesky_train_train = Eigen::MatrixXd();
    m_cov_query_train = Eigen::MatrixXd();
}


void GPSimple::append_training_data(
        Eigen::MatrixXd const&          data,
        Eigen::VectorXd const&          labels
)
{
    if(data.rows() != labels.rows())
    {
        throw DimensionException("Row count of new data does not match");
    }
    if(m_training.rows() > 0 && data.cols() != m_training.cols())
    {
        throw DimensionException("Dimensionality of old and new data does not match");
    }

    int new_rowcount = m_training.rows() + data.rows();

    // Resize data storage
    if(m_training.cols() == 0)
    {
        m_training.conservativeResize(data.rows(), data.cols());
    }
    else
    {
        m_training.conservativeResize(new_rowcount, m_training.cols());
    }
    m_labels.conservativeResize(new_rowcount);

    // Append new data at the end
    m_training.bottomRows(data.rows()) = data;
    m_labels.tail(labels.rows()) = labels;

    //Check if cov_func exists
    if(get_cov_func())
    {
        // Update query to training data covariance matrix
        if(get_query_points().rows() > 0)
        {
            m_cov_query_train.conservativeResize(
                    m_cov_query_train.rows() + data.rows(),
                    get_query_points().rows()
            );
            m_cov_query_train.bottomRows(data.rows()) =
                    get_cov_func()->cov(data, get_query_points());
        }

        // Update the Cholesky factorization
        int N = data.rows();
        int size = m_cholesky_train_train.rows();
        Eigen::VectorXd noise_std = get_noise_func()->noise(data);
        m_cholesky_train_train.conservativeResize(size+N, size+N);
        m_cholesky_train_train.rightCols(N) = get_cov_func()->cov(m_training, data);
        m_cholesky_train_train.bottomRightCorner(N, N).noalias() +=
            noise_std[0]*noise_std[0]*Eigen::MatrixXd::Identity(N, N);
        m_cholesky_train_train
            .topLeftCorner(size, size)
            .triangularView<Eigen::Upper>()
            .transpose()
            .solveInPlace(m_cholesky_train_train.topRightCorner(size,N));
        m_cholesky_train_train.bottomRightCorner(N, N) = (
                m_cholesky_train_train.bottomRightCorner(N,N) -
                m_cholesky_train_train.topRightCorner(size,N).transpose() *
                m_cholesky_train_train.topRightCorner(size,N)).llt().matrixU();
    }
}

void GPSimple::remove_last_training_point()
{
    // Check if there is something to remove
    if(m_training.rows() == 0 || m_labels.rows() == 0)
    {
        throw DimensionException("There is no old data to remove");
    }

    // Remove data and label
    m_training.conservativeResize(m_training.rows()-1, m_training.cols());
    m_labels.conservativeResize(m_labels.rows()-1);

    // Downdate covariance between query and training data
    m_cov_query_train.conservativeResize(
            m_cov_query_train.rows()-1, m_query.rows()
    );

    // Downdate the Cholesky factorization
    m_cholesky_train_train.conservativeResize(
            m_cholesky_train_train.rows()-1,
            m_cholesky_train_train.cols()-1
    );
}

void GPSimple::set_training_labels(Eigen::VectorXd const& labels)
{
    if(labels.rows() != m_training.rows())
    {
        throw DimensionException("Dimension mismatch between training data "
                             "and training labels.");
    }
    AbstractGaussianProcess::set_training_labels(labels);
}

void GPSimple::clear_query_points()
{
    AbstractGaussianProcess::clear_query_points();
    m_cov_query_train = Eigen::MatrixXd();
    m_cov_query_query = Eigen::MatrixXd();
}


void GPSimple::recalculate_cov_train_train()
{
    //Check if cov_func exists
    if(m_cov_func)
    {
        if(m_training.rows() > 0 && m_training.cols() > 0)
        {
            Eigen::VectorXd noise_std = get_noise_func()->noise(m_training);
            m_cov_train_train =
                m_cov_func->cov(m_training) +
                    noise_std[0] * noise_std[0] *
                Eigen::MatrixXd::Identity(m_training.rows(), m_training.rows());
            m_cholesky_train_train =
                m_cov_train_train.selfadjointView<Eigen::Upper>().llt().matrixU();
        }
        else
        {
            clear_training_data();
        }
    }
}

GPResult GPSimple::inference()
{
    Eigen::VectorXd kss(m_query.rows());
    for(int i=0; i<m_query.rows(); ++i)
    {
        kss(i) = get_cov_func()->cov(m_query.row(i)).value();
    }

    Eigen::MatrixXd ks = get_cov_func()->cov(get_training_data(), m_query);
    Eigen::VectorXd m = get_mean_func()->mean(get_training_data());

    // Compute inverse
    Eigen::MatrixXd L = get_cholesky_train_train();
    Eigen::VectorXd alpha = L.triangularView<Eigen::Upper>()
        .transpose()
        .solve(get_training_labels() - m);
    L.triangularView<Eigen::Upper>().solveInPlace(alpha);

    // Compute predictive mean and variance
    GPResult result;
    Eigen::VectorXd ms = get_mean_func()->mean(m_query);
    result.mean = ms + (ks.transpose() * alpha);  //Rasmussen 2.38
    Eigen::MatrixXd v = L.triangularView<Eigen::Upper>().transpose().solve(ks);
    result.variance =
        kss - v.array().square().colwise().sum().transpose().matrix();

    return result;
}

double GPSimple::evaluate_goal_function(
        ParametricFunction::ParamVec_t const& gp_params
)
{
    unsigned int mean_init = 0;
    unsigned int mean_end = get_mean_func()->get_nr_of_free_params();

    unsigned int cov_init = mean_end;
    unsigned int cov_end = cov_init +
                            get_cov_func()->get_nr_of_free_params();

    unsigned int noise_init = cov_end;
    unsigned int noise_end = noise_init +
                            get_noise_func()->get_nr_of_free_params();

    //First parameters are from the mean function
    ParametricFunction::ParamVec_t mean_params(gp_params.begin()+mean_init,
                                               gp_params.begin()+mean_end);

    //Next parameters are from the cov function
    ParametricFunction::ParamVec_t cov_params(gp_params.begin()+cov_init,
                                              gp_params.begin()+cov_end);

    //Final parameters are from the noise function
    ParametricFunction::ParamVec_t noise_params(gp_params.begin()+noise_init,
                                                gp_params.begin()+noise_end);

    if(mean_params.size() +
       cov_params.size() +
       noise_params.size() != gp_params.size())
    {
        throw DimensionException("GPSimple:evaluate_goal_function, "
                                 "size of parameters is not what is expected.");
    }

    switch(m_goal_func_type)
    {
        case GF_LML:
        {
            unsigned int N = get_training_data().rows();

            Eigen::MatrixXd K = get_cov_func()->cov(get_training_data(),
                                                    cov_params);

            Eigen::VectorXd noise_val = get_noise_func()->noise(
                                                    get_training_data(),
                                                    noise_params);

            Eigen::VectorXd m = get_mean_func()->mean(get_training_data(),
                                                   mean_params);

            Eigen::MatrixXd L = (K +
                    (noise_val[0]*noise_val[0])*Eigen::MatrixXd::Identity(N,N));

            Eigen::VectorXd y = get_training_labels() - m;

            Eigen::LLT<Eigen::MatrixXd> llt;
            llt.compute(L);

            Eigen::VectorXd alpha;
            alpha = llt.solve(y);

            double llt_diagonal_sum = 0;

            for(unsigned int i = 0;
                i < (unsigned int)llt.matrixLLT().rows();
                i++)
            {
                llt_diagonal_sum += log(llt.matrixLLT()(i,i));
            }

            Eigen::VectorXd lml1_vec = -0.5*((y.transpose())*alpha);
            double lml1 = lml1_vec[0];
            double lml2 = - llt_diagonal_sum;
            double lml3 = -0.5*N*log(2*M_PI);
            double lml = lml1+lml2+lml3;

            return lml;
            break;
        }
        case GF_LOOCV:
        {
            throw(BaseException("Leave One Out Cross Validation not implemented"
                            "yet."));
            break;
        }
    }
    throw(BaseException("Goal function not recognized."));
    return 0;
}

void GPSimple::recalculate_cov_query_train()
{
    //Check if cov_func exists
    if(m_cov_func)
    {
        if(m_training.rows() > 0 && m_query.rows() > 0)
        {
            m_cov_query_train = m_cov_func->cov(m_training, m_query);
        }
    }
}


void GPSimple::recalculate_cov_query_query()
{
    //Check if cov_func exists
    if(m_cov_func)
    {
        if(m_query.rows() > 0)
        {
            m_cov_query_query = Eigen::VectorXd(m_query.rows());

            for(int i=0; i<m_query.rows(); ++i)
            {
                m_cov_query_query(i) = m_cov_func->cov(m_query.row(i)).value();
            }
        }
    }
}

} /* gp */
