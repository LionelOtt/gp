#ifndef __GP_GP_SUBSET_OF_REGRESSORS_H__
#define __GP_GP_SUBSET_OF_REGRESSORS_H__

#include <gp/gp/abstract_gaussian_process.h>

namespace gp
{

/**
 * \brief Gaussian Process Subset of Regressors.
 *
 * This kind of gaussian process is an approximation of the full solution of a
 * GP. This code is based on chapter 8.3.1 of Rasmussen and Williams,
 * Gaussian Process for Machine Learning, 2006. The idea is to avoid the
 * inversion of a large matrix, originally calculated over the entire training
 * dataset. For this case, a set of induced points are chosen to be the
 * subset of regressors. All the training points are "projected" to the
 * subset of regressors and the learning and prediction becomes faster.
 */

class GPSR
        : public AbstractGaussianProcess
{
    public:

        GPSR();

        GPSR(boost::shared_ptr<AbstractCovarianceFunction> cov_func,
                 boost::shared_ptr<AbstractMeanFunction> mean_func,
                 boost::shared_ptr<AbstractNoiseFunction> noise_func);

        /**
         * \brief Sets the hyper parameters of the entire gp.
         *
         * \param params set of hyper parameters to use
         */
        void set_gp_hyper_params(
                AbstractCovarianceFunction::ParamVec_t const& params);

        /**
         * \brief Sets the hyper parameters of the covariance function.
         *
         * \param params set of hyper parameters to use
         */
        void set_free_gp_hyper_params(
                AbstractCovarianceFunction::ParamVec_t const& params);

        /**
         * \brief Sets the data and labels of the GP to the given values.
         *
         * Each row of the data matrix is assumed to contain a single
         * example  with the same row of the label vector containing the
         * associated label.
         *
         * \param data the new training data to use
         * \param labels the labels corresponding to the data entries
         */
        void set_training_data(
                Eigen::MatrixXd const&  data,
                Eigen::VectorXd const&  labels
        );

        /**
         * \brief Appends to given data to the existing data.
         *
         * \param data the data entries to add
         * \param labels the labels of the entries to be added
         */
        void append_training_data(
                Eigen::MatrixXd const&  data,
                Eigen::VectorXd const&  labels
        );

        /**
         * \brief Removes the last data point.
         */
        void remove_last_training_point();

        /**
         * \brief Sets the labels of the training data.
         *
         * This method only changes the labels without changing the training
         * data itself.
         *
         * \param labels new labels for the training data
         */
        void set_training_labels(Eigen::VectorXd const& labels);

        /**
         * \brief Clears the query points, and clears the induced points correlation matrix.
         *
         * \param labels new labels for the training data
         */
        void clear_query_points();

        void clear_training_data();

        Eigen::MatrixXd const& get_training_data() const;

        Eigen::MatrixXd & get_training_data();

        /**
         * \brief Sets the inducing points to specifi data.
         *
         * \param inducing_points Points where the correlations are calculated.
         */
        void set_inducing_points(Eigen::MatrixXd const& inducing_points);

        Eigen::MatrixXd const& get_induced_points();

        /**
         * \brief Precalculates matrixes and covariances to be more efficient.
         *
         *  This function precalculates all the information that is not related
         *  to the query. Such that for a new query only inference can be called,
         *  and all the precalculations can be reused.
         */
        void solve();

        /**
         * \brief Performs inference and returns mean and variance.
         *
         * \param query_points the qyery points to infer values for
         * \return inference results
         */
        GPResult inference();

        double evaluate_goal_function(
                ParametricFunction::ParamVec_t const& gp_params);

    private:
        /**
         * \brief Recomputes the covariance matrixes.
         */
        void recalculate_covariances();

    private:
        //! Training data
        Eigen::MatrixXd                 m_training;

        //! Alpha vector
        Eigen::VectorXd                 m_alpha;

        //! Inducing points (Set I in the textbook)
        Eigen::MatrixXd                 m_inducing_points;

        //! Covariance matrix between inducing points
        Eigen::MatrixXd                 m_cov_ind_ind;

        //! Covariance matrix between train and inducing points
        Eigen::MatrixXd                 m_cov_train_ind;

        //! Covariance matrix between train and inducing points
        Eigen::MatrixXd                 m_cov_query_ind;

        //! K hat inverse
        Eigen::MatrixXd                 m_k_hat_inv;

        //! K hat
        Eigen::MatrixXd                 m_k_hat;

};

} /* gp */

#endif /* __GP_GP_SUBSET_OF_REGRESSORS_H__ */
