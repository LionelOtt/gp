#ifndef __GP_ERROR_HPP__
#define __GP_ERROR_HPP__

#include <iostream>
#include <exception>
#include <string>


namespace gp
{
    
/**
 * \brief Base class of the exception hierarchy.
 */
class BaseException
        :public std::exception
{
    public:
        /**
         * \brief Creates a new BaseException instance.
         *
         * \param err the error message
         */
        BaseException(std::string const& err)
            :   m_error(err)
        {}

        ~BaseException() throw (){}

        /**
         * \brief Returns the stored error string.
         */
        virtual const char* what() const throw()
        {
            return m_error.c_str();
        }

    private:
        //! Error string of this exception
        std::string                     m_error;
};

/**
 * \brief Error thrown in relation with the dimension of data.
 */
class DimensionException : public BaseException
{
    public:
        /**
         * \brief Creates a new DimensionException instance.
         *
         * \param err the error message
         */
        DimensionException(std::string const& err)
            :   BaseException(err)
        {}
};

/**
 * \brief Error thrown in relation with parameter usage.
 */
class ParameterException : public BaseException
{
    public:
        /**
         * \brief Creates a new ParameterException instance.
         *
         * \param err the error message
         */
        ParameterException(std::string const& err)
            :   BaseException(err)
        {}
};

/**
 * \brief Error thrown in relation with the optimiser usage.
 */
class OptimiserException : public BaseException
{
    public:
        /**
         * \brief Creates a new OptimiserException instance.
         *
         * \param err the error message
         */
        OptimiserException(std::string const& err)
            :   BaseException(err)
        {}
};

/**
 * \brief Error thrown in relation with a derivative requested usage.
 */
class DerivativeException : public BaseException
{
    public:
        /**
         * \brief Creates a new ParameterException instance.
         *
         * \param err the error message
         */
        DerivativeException(std::string const& err)
            :   BaseException(err)
        {}
};

} /* gp */


#endif /* __GP_ERROR_HPP__ */
