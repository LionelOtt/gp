GP Library
==========

This is a C++ Gaussian Process library.


Building
--------

First make sure you install all dependencies:

```bash
sudo apt-get install build-essential
sudo apt-get install libnlopt-dev
sudo apt-get install libboost-system1.55-dev
sudo apt-get install libboost-date-time1.55-dev
sudo apt-get install libboost-filesystem1.55-dev
sudo apt-get install libeigen3-dev
sudo apt-get install libtbb-dev
sudo apt-get install cmake
sudo apt-get install doxygen
```

If you need mor information building, running and resources. Please visit the repository [wiki](https://bitbucket.org/LionelOtt/gp/wiki/Home).