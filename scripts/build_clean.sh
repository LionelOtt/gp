if [ -d "build" ]
then
    rm -r build
fi
if [ -d "lib" ]
then
    rm -r lib
fi
if [ -d "bin" ]
then
    rm -r bin
fi
if [ -f src/CMakeLists.user ]
then
    rm src/CMakeLists.user
fi

