#!/usr/bin/env python

import os
import sys

import numpy as np
import pylab as plt


def sort_points_2(x, y):
    points = sorted(zip(x, y), key=lambda pt : pt[0])
    x = [pt[0] for pt in points]
    y = [pt[1] for pt in points]
    return np.array(x), np.array(y)

def sort_points_3(x, y, o):
    points = sorted(zip(x, y, o), key=lambda pt : pt[0])
    x = [pt[0] for pt in points]
    y = [pt[1] for pt in points]
    o = [pt[2] for pt in points]
    return np.array(x), np.array(y), np.array(o)

def read_line_data_2(fpath):
    lines = open(fpath).readlines()
    x = [float(v) for v in lines[0].split()]
    y = [float(v) for v in lines[1].split()]
    return x, y

def read_line_data_3(fpath):
    lines = open(fpath).readlines()
    x = [float(v) for v in lines[0].split()]
    y = [float(v) for v in lines[1].split()]
    o = [float(v) for v in lines[2].split()]
    return x, y, o

def plot_gp(train, pred):
    x1, y1 = sort_points_2(*read_line_data_2(train))
    x2, y2, var = sort_points_3(*read_line_data_3(pred))

    sigma = 2*np.sqrt(var);

    plt.clf()
    plt.plot(x1, y1, '+k')
    plt.plot(x2, y2, '-')
    plt.fill_between(x2, y2+sigma, y2-sigma, alpha=0.1)
    plt.grid()
    plt.show()

def main():
    # Check for a valid command
    if len(sys.argv) == 1:
        usage()
        return 1

    command = sys.argv[1]
    if command == "gp1d":
        plot_gp(sys.argv[2], sys.argv[3])


if __name__ == "__main__":
    sys.exit(main())
